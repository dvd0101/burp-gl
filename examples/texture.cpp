#include "burpgl/attributes.h"
#include "burpgl/buffers.h"
#include "burpgl/draw.h"
#include "burpgl/program.h"
#include "burpgl/shader.h"
#include "burpgl/textures/texture.h"
#include "burpgl/vao.h"
#include "context/context.h"
#include "context/debug.h"
#include <fmt/format.h>
#include <glbinding/gl/gl.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/mat4x4.hpp>
#include <vector>

using namespace gl;

namespace b = burpgl;

using flat_tx = b::textures::texture<STYPE("flat_tx"), GL_TEXTURE_2D, GL_RGBA8>;

using position  = b::attribute<STYPE("position"), 0, GLfloat[2]>;
using tex_coord = b::attribute<STYPE("tex_coord"), 1, GLfloat[2]>;
using mvp       = b::uniform<STYPE("mvp"), gl::GLfloat[4][4], 1>;
using material  = b::uniform<STYPE("material"), flat_tx, 4>;

constexpr auto buffer_features = static_cast<GLbitfield>(BufferStorageMask::GL_DYNAMIC_STORAGE_BIT);

using data_buffer       = b::array_buffer<buffer_features, position, tex_coord>;

namespace shaders {
	using vao      = b::vao<GL_TRIANGLES, data_buffer>;
	using vertex   = b::shaders::vertex<position, tex_coord, material, mvp>;
	using fragment = b::shaders::fragment<>;

	const char* vsh_source = R"(
#version 410

in vec2 position;
in vec2 tex_coord;
uniform mat4 mvp;
out vec2 material_coord;

void main() {
    gl_Position = mvp * vec4(position, 0, 1);
    material_coord = tex_coord;
}
	)";

	const char* fsh_source = R"(
#version 410

in vec2 material_coord;
out vec4 out_color;
uniform sampler2D material[4];

void main() {
	out_color = texture(material[0], material_coord);
}
	)";
}

struct rgb {
	uint8_t red, green, blue;
};

namespace burpgl {
	namespace textures {
		template <>
		struct pixel_transfer<rgb> {
			static const GLenum pixel_format = GL_RGB;
			static const GLenum pixel_type   = GL_BYTE;
		};
	}
}

struct Point {
	GLfloat p[2];
	GLfloat uv[2];
};

std::array<GLfloat[2], 3> teq(Point c, float length) {
	const float L = length;
	const float H = 1.7321 * L / 2;
	const float R = L / 1.7321;
	return {{{c.p[0], c.p[1] + R}, {c.p[0] - L / 2, c.p[1] - H + R}, {c.p[0] + L / 2, c.p[1] - H + R}}};
}

std::vector<Point> model() {
	std::vector<Point> points;
	auto copy = [&](const std::array<GLfloat[2], 3>& v) {
		points.push_back(Point{v[0][0], v[0][1], 0, 0});
		points.push_back(Point{v[1][0], v[1][1], 0, 1});
		points.push_back(Point{v[2][0], v[2][1], 1, 1});
	};
	copy(teq({-0.5, 0.5}, 0.3));
	copy(teq({0.5, 0.5}, 0.3));
	copy(teq({-0.5, -0.5}, 0.3));
	copy(teq({0.5, -0.5}, 0.3));
	return points;
}

int main() {
	glcontext::Context ctx({768, 768}, glcontext::Mode::windowed);
	glcontext::try_log_debug_messages();
	fmt::print("OPENGL: {}\n", ctx.name());

	flat_tx materials[4] = {{b::textures::mipmap{1}, 2u, 2u},
	                        {b::textures::mipmap{1}, 2u, 2u},
	                        {b::textures::mipmap{1}, 2u, 2u},
	                        {b::textures::mipmap{1}, 2u, 2u}};
	{
		// load the textures to opengl
		std::array<rgb, 4> colors;
		colors[0] = {255, 0, 0};
		colors[1] = {255, 0, 0};
		colors[2] = {255, 0, 0};
		colors[3] = {255, 0, 0};
		materials[0].unpack(colors);

		colors[0] = {0, 255, 0};
		colors[1] = {0, 255, 0};
		colors[2] = {0, 255, 0};
		colors[3] = {0, 255, 0};
		materials[1].unpack(colors);

		colors[0] = {0, 0, 255};
		colors[1] = {0, 0, 255};
		colors[2] = {0, 0, 255};
		colors[3] = {0, 0, 255};
		materials[2].unpack(colors);

		colors[0] = {255, 0, 255};
		colors[1] = {255, 0, 255};
		colors[2] = {255, 0, 255};
		colors[3] = {255, 0, 255};
		materials[3].unpack(colors);
	}

	auto points = model();
	data_buffer vertices({reinterpret_cast<uint8_t*>(points.data()), points.size() * sizeof(Point)});

	shaders::vertex vsh;
	vsh << shaders::vsh_source;

	shaders::fragment fsh;
	fsh << shaders::fsh_source;

	using b::shaders::compile;
	using b::shaders::link;
	auto prg = link(compile(vsh), compile(fsh));

	shaders::vao data;
	data.bind(vertices);

	auto dc = b::draw::context(prg, data);

	dc.texture(materials[0], 0);
	//dc.texture(materials[0], 1);
	//dc.texture(materials[0], 2);
	//dc.texture(materials[0], 3);

	glClearColor(0., 0., 0.3, 1.);
	draw_forever(ctx, [&]() {
		static int frames  = 0;
		static float angle = 0.0;
		const auto r       = glm::rotate(glm::mat4{1.0}, angle, glm::vec3{0, 0, 1});
		angle += 0.02;

		auto ptr = reinterpret_cast<const std::array<float, 16>*>(glm::value_ptr(r));
		dc.program().set_uniform<mvp>(*ptr);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		dc.draw_array();

		return true;
		return ++frames <= 60;
	});
}
