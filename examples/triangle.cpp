#include "burpgl/attributes.h"
#include "burpgl/buffers.h"
#include "burpgl/draw.h"
#include "burpgl/program.h"
#include "burpgl/shader.h"
#include "burpgl/vao.h"
#include "context/context.h"
#include "context/debug.h"
#include <fmt/format.h>
#include <glbinding/gl/gl.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/mat4x4.hpp>
#include <vector>

using namespace gl;

namespace b = burpgl;

using position = b::attribute<STYPE("position"), 0, GLfloat[2]>;
using color    = b::attribute<STYPE("color"), 1, GLfloat[3]>;
using mvp      = b::uniform<STYPE("mvp"), gl::GLfloat[4][4], 1>;

constexpr auto buffer_features = static_cast<GLbitfield>(BufferStorageMask::GL_DYNAMIC_STORAGE_BIT);

using data_buffer       = b::array_buffer<buffer_features, position, color>;
using elements_indicies = b::element_array_buffer<buffer_features, GL_UNSIGNED_INT>;

namespace shaders {
	using vao      = b::vao<GL_TRIANGLES, data_buffer, elements_indicies>;
	using vertex   = b::shaders::vertex<position, color, mvp>;
	using fragment = b::shaders::fragment<>;

	const char* vsh_source = R"(
#version 410

in vec2 position;
in vec3 color;
out vec3 vertex_color;

uniform mat4 mvp;

void main() {
    gl_Position = mvp * vec4(position, 0, 1);
    vertex_color = color;
}
	)";

	const char* fsh_source = R"(
#version 410

out vec4 out_color;
in vec3 vertex_color;

void main() {
	out_color = vec4(vertex_color, 1.0);
}
	)";
}

struct Point {
	GLfloat p[2];
	GLfloat c[3];
};

int main() {
	glcontext::Context ctx({768, 768}, glcontext::Mode::windowed);
	glcontext::try_log_debug_messages();
	fmt::print("OPENGL: {}\n", ctx.name());

	data_buffer vertices(3 * sizeof(Point));
	{
		const float L = 0.8;
		const float H = 1.7321 * L / 2;
		const float R = L / 1.7321;
		// clang-format off
		std::vector<Point> points{
			{ { 0.f, 0.f + R}, {1., 0., 0.}},
			{ {-L/2, -H  + R}, {0., 1., 0.}},
			{ { L/2, -H  + R}, {0., 0., 1.}},
		};
		// clang-format on

		b::update(vertices,
		          0,
		          gsl::span<uint8_t>{reinterpret_cast<uint8_t*>(points.data()),
		                             gsl::narrow<int>(points.size() * sizeof(Point))});
	}
	elements_indicies indicies(std::vector<uint32_t>{0, 1, 2});

	shaders::vertex vsh;
	vsh << shaders::vsh_source;

	shaders::fragment fsh;
	fsh << shaders::fsh_source;

	using b::shaders::compile;
	using b::shaders::link;
	auto prg = link(compile(vsh), compile(fsh));

	shaders::vao data;
	data.bind(vertices);
	data.bind(indicies);

	auto dc = b::draw::context(prg, data);

	glClearColor(0., 0., 0.3, 1.);
	draw_forever(ctx, [&]() {
		static int frames  = 0;
		static float angle = 0.0;
		const auto r       = glm::rotate(glm::mat4{1.0}, angle, glm::vec3{0, 0, 1});
		angle += 0.02;

		auto ptr = reinterpret_cast<const std::array<float, 16>*>(glm::value_ptr(r));
		dc.program().set_uniform<mvp>(*ptr);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		dc.draw_array();

		return ++frames <= 60;
	});

	glClearColor(0.3, 0., 0., 1.);
	draw_forever(ctx, [&]() {
		static int frames  = 0;
		static float angle = 0.0;
		const auto r       = glm::rotate(glm::mat4{1.0}, angle, glm::vec3{0, 0, 1});
		angle += 0.02;

		auto ptr = reinterpret_cast<const std::array<float, 16>*>(glm::value_ptr(r));
		dc.program().set_uniform<mvp>(*ptr);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		dc.draw_elements();

		return ++frames <= 60;
	});

	return 0;
}
