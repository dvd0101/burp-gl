#pragma once
#include "attributes.h"
#include "gl_impl.h"

namespace burpgl {
	namespace vertex_array_object {
		// Depending on the OpenGL version there are two ways to bind an
		// attribute to a VAO.
		//
		// Until OpenGL 4.3, we must use the glVertexAttrib*Pointer functions.
		// These functions are used to both set the format of the attribute
		// that the source of the data
		//
		// Since OpenGL 4.3, we can use three different functions.
		//
		// 1. glVertexAttrib*Format actually this is a family of functions;
		// they are used to set the format of the attribute
		//
		// 2. glVertexAttribBinding is used to map an attribute to a buffer;
		// the mapping is between an attribute location and a name (an integer
		// as is common in OpenGL) *local* to the VAO
		//
		// 3. glBindVertexBuffer is used to set the source of the data; it maps
		// a buffer name to the *local* name associated with one or more
		// attributes.
		//
		// The new functions are used to increase the performance; because
		// while changing the source buffer (function 3) is a cheap operation,
		// altering the format of an attribute (functions 1 and 2) is very
		// expensive.
		//
		// Further readings:
		// http://stackoverflow.com/questions/37972229
		// https://www.khronos.org/opengl/wiki/Vertex_Specification
		//
		// According to which version of OpenGL we are using two different sets
		// of function are defined

		namespace details {
			template <attributes::classtype ClassType, typename Attribute>
			struct bind_format;

			template <typename Attribute>
			struct bind_format<attributes::classtype::FLOAT, Attribute> {
				static void call(GL::GLuint relativeoffset) {
					GL::glVertexAttribFormat(
					    Attribute::index, Attribute::components, Attribute::type_data::value, false, relativeoffset);
				}
			};

			template <typename Attribute>
			struct bind_format<attributes::classtype::INTEGER, Attribute> {
				static void call(GL::GLuint relativeoffset) {
					GL::glVertexAttribIFormat(
					    Attribute::index, Attribute::components, Attribute::type_data::value, relativeoffset);
				}
			};

			template <typename Attribute>
			struct bind_format<attributes::classtype::DOUBLE, Attribute> {
				static void call(GL::GLuint relativeoffset) {
					GL::glVertexAttribLFormat(
					    Attribute::index, Attribute::components, Attribute::type_data::value, relativeoffset);
				}
			};
		}

		template <typename Attribute>
		void bind_attribute_format(GL::GLuint relativeoffset) {
			// clang-format off
			if constexpr (!gl_vertex_attrib_format) {
				static_assert(mpl::always_false<void>, "This method requires the vertex format api (OepnGL >= 4.3)");
			}
			// clang-format on
			constexpr auto ct = attributes::attribute_type<typename Attribute::type>::class_type;
			details::bind_format<ct, Attribute>::call(relativeoffset);
		}

		inline void bind_attribute_buffer(GL::GLuint index, GL::GLuint buffer_local_name) {
			GL::glVertexAttribBinding(index, buffer_local_name);
		}

		inline void bind_source_buffer(GL::GLuint buffer_local_name, GL::GLuint buffer_name, GL::GLint offset,
		                               size_t buffer_stride) {
			GL::glBindVertexBuffer(buffer_local_name, buffer_name, offset, buffer_stride);
		}

		namespace details {
			template <attributes::classtype ClassType, typename Attribute>
			struct bind_attribute;

			template <typename Attribute>
			struct bind_attribute<attributes::classtype::FLOAT, Attribute> {
				static void call(GL::GLuint stride, GL::GLuint offset) {
					GL::glVertexAttribPointer(Attribute::index,
					                          Attribute::components,
					                          Attribute::type_data::value,
					                          false,
					                          stride,
					                          reinterpret_cast<const void*>(offset));
				}
			};

			template <typename Attribute>
			struct bind_attribute<attributes::classtype::INTEGER, Attribute> {
				static void call(GL::GLuint stride, GL::GLuint offset) {
					GL::glVertexAttribIPointer(Attribute::index,
					                           Attribute::components,
					                           Attribute::type_data::value,
					                           stride,
					                           reinterpret_cast<const void*>(offset));
				}
			};

			template <typename Attribute>
			struct bind_attribute<attributes::classtype::DOUBLE, Attribute> {
				static void call(GL::GLuint stride, GL::GLuint offset) {
					GL::glVertexAttribLPointer(Attribute::index,
					                           Attribute::components,
					                           Attribute::type_data::value,
					                           stride,
					                           reinterpret_cast<const void*>(offset));
				}
			};
		}

		template <typename Attribute>
		void bind_whole_attribute(GL::GLuint stride, GL::GLuint offset) {
			// clang-format off
			if constexpr (gl_vertex_attrib_format) {
				static_assert(mpl::always_false<void>, "This method must not be used if the vertex format api is available");
			}
			// clang-format on
			constexpr auto ct = attributes::attribute_type<typename Attribute::type>::class_type;
			details::bind_attribute<ct, Attribute>::call(stride, offset);
		}
	}
}
