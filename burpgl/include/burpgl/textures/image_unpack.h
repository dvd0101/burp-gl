#pragma once
#include "../details/mpl.h"
#include "image_selection.h"

namespace burpgl {
	namespace textures {
		template <typename T>
		struct pixel_transfer
		/*
		 * pixel_transfer is a template that conveys the parameters about the
		 * format of a pixel transfer.
		 *
		 * The default implementation unconditionally raises a static assert;
		 * it must be specialized for every used type.
		 *
		 * Example:
		 *
		 * You store the pixel data in the following type:
		 *
		 * struct rgb {
		 *   uint8_t r, g, b;
		 * };
		 *
		 * The you must provide the following specialization:
		 *
		 * namespace burpgl {
		 *   namespace textures {
		 *     template<>
		 *     struct pixel_transfer<rgb> {
		 *       static const GLenum pixel_format = GL_RGB;
		 *       static const GLenum pixel_type   = GL_UNSIGNED_BYTE;
		 *     };
		 *   }
		 * }
		 */
		{
			/*
			static const GL::GLenum pixel_format;
			static const GL::GLenum pixel_type;
			*/
			static_assert(mpl::always_false<T>, "You must specialize pixel_transfer<> for your types");
		};

		namespace details {
			template <bool HasMipmaps, bool HasArrayLayers, bool HasCubemapFaces>
			struct unpacker_base {
				using index = image_index<HasMipmaps, HasArrayLayers, HasCubemapFaces>;

				// clang-format off
				template <typename T>
				struct pt_details {
					constexpr static GL::GLenum pixel_format = pixel_transfer<T>::pixel_format;
					static_assert(
						pixel_format == GL::GL_RED             ||
						pixel_format == GL::GL_GREEN           ||
						pixel_format == GL::GL_BLUE            ||
						pixel_format == GL::GL_RG              ||
						pixel_format == GL::GL_RGB             ||
						pixel_format == GL::GL_BGR             ||
						pixel_format == GL::GL_RGBA            ||
						pixel_format == GL::GL_BGRA            ||
						pixel_format == GL::GL_DEPTH_COMPONENT ||
						pixel_format == GL::GL_STENCIL_INDEX   ||
						pixel_format == GL::GL_DEPTH_STENCIL, "unsupported pixel format");


					constexpr static GL::GLenum pixel_type = pixel_transfer<T>::pixel_type;
					static_assert(
						pixel_type == GL::GL_UNSIGNED_BYTE  ||
						pixel_type == GL::GL_BYTE           ||
						pixel_type == GL::GL_UNSIGNED_SHORT ||
						pixel_type == GL::GL_SHORT          ||
						pixel_type == GL::GL_UNSIGNED_INT   ||
						pixel_type == GL::GL_INT            ||
						pixel_type == GL::GL_HALF_FLOAT     ||
						pixel_type == GL::GL_FLOAT, "unsupported pixel type");

					const void* data;
				};
				// clang-format on

				template <typename Container>
				static auto pixel_transfer_details(const Container& dataset)
				/*
				 * pixel_transfer_details returns the needed details to start
				 * the pixel transfer from the container to opengl.
				 *
				 * It returns an instance of `pt_details` that contains:
				 *  - the pixel format
				 *  - the pixel type
				 *  - the address of the client memory from where to start the
				 *  transfer
				 *
				 * In order to derive the pixel format and type, a
				 * specialization of `pixel_transfer<>` is used; so if, for
				 * example, Container is a `std::vector<rgb>`,
				 * `pixel_transfer<rgb>` must also exist.
				 *
				 * `Container` must have:
				 *  1. A nested type `value_type` that is the type of the elements.
				 *  2. A `.data()` method that returns a pointer to the
				 *  contiguos memory area.
				 */
				{
					using Element = typename Container::value_type;
					using Details = pt_details<Element>;
					return Details{dataset.data()};
				}
			};

			/*
			 * unpacker is an implementation details.
			 *
			 * It is a template class that knows how to unpack (ie it copy from
			 * the client memory to opengl) some bytes into a texture.
			 *
			 * OpenGL provides three functions to unpack to an immutable
			 * textures, glTexSubImage{1,2,3}D, but their use changes according
			 * to the texture type.
			 *
			 * The three template parameters are used to divide the textures in
			 * five groups (just five because not all the combinations are
			 * useful) in order to offer an homogeneous interface to the
			 * calling code.
			 *
			 */
			template <bool HasMipmaps, bool HasArrayLayers, bool HasCubemapFaces>
			struct unpacker;

			template <>
			struct unpacker<true, true, true> : unpacker_base<true, true, true>
			// GL_TEXTURE_CUBE_MAP_ARRAY
			{

				template <GL::GLenum TextureType, typename T>
				static void unpack(index, region<2>, const T&) {
					static_assert(TextureType == GL::GL_TEXTURE_CUBE_MAP_ARRAY, "Invalid/unsupported texture type");
					// glTexSubImage3D
				}
			};

			template <>
			struct unpacker<true, false, true> : unpacker_base<true, false, true>
			// GL_TEXTURE_CUBE_MAP
			{
				template <GL::GLenum TextureType, typename T>
				static void unpack(index, region<2>, const T&) {
					static_assert(TextureType == GL::GL_TEXTURE_CUBE_MAP, "Invalid/unsupported texture type");
					// glTexSubImage2D
				}
			};

			template <>
			struct unpacker<true, true, false> : unpacker_base<true, true, false>
			// GL_TEXTURE_1D_ARRAY
			// GL_TEXTURE_2D_ARRAY
			{
				template <GL::GLenum TextureType, typename T>
				static void unpack(index, region<1>, const T&) {
					// glTexSubImage2D
				}

				template <GL::GLenum TextureType, typename T>
				static void unpack(index, region<2>, const T&) {
					static_assert(TextureType != GL::GL_TEXTURE_2D_MULTISAMPLE_ARRAY,
					              "Multisample textures cannot be updated from client data");
					// glTexSubImage3D
				}
			};

			template <>
			struct unpacker<true, false, false> : unpacker_base<true, false, false>
			// GL_TEXTURE_1D
			// GL_TEXTURE_2D
			// GL_TEXTURE_3D
			{
				template <GL::GLenum TextureType, typename T>
				static void unpack(index, region<1>, const T&) {
					// glTexSubImage1D
				}

				template <GL::GLenum TextureType, typename T>
				static void unpack(index ix, region<2> r, const T& dataset) {
					static_assert(TextureType != GL::GL_TEXTURE_2D_MULTISAMPLE,
					              "Multisample textures cannot be updated from client data");
					auto pt = pixel_transfer_details(dataset);
					GL::glTexSubImage2D(TextureType,
					                    ix.mipmap_level,
					                    r.origin.x,
					                    r.origin.y,
					                    r.size.width,
					                    r.size.height,
					                    pt.pixel_format,
					                    pt.pixel_type,
					                    pt.data);
				}

				template <GL::GLenum TextureType, typename T>
				static void unpack(index, region<3>, const T&) {
					// glTexSubImage3D
				}
			};

			template <>
			struct unpacker<false, false, false> : unpacker_base<false, false, false>
			// GL_TEXTURE_RECTANGLE
			// GL_TEXTURE_BUFFER
			{
				template <GL::GLenum TextureType, typename T>
				static void unpack(index, region<1>, const T&) {
					// glTexSubImage1D
				}

				template <GL::GLenum TextureType, typename T>
				static void unpack(index, region<2>, const T&) {
					// glTexSubImage2D
				}
			};
		}
	}
}
