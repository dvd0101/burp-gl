#pragma once
#include "image_features.h"
#include "image_selection.h"
#include "image_storage.h"
#include "image_unpack.h"
#include <array>
#include <glbinding/gl41core/enum.h>

namespace burpgl {
	namespace textures {
		template <GL::GLenum TextureType, typename ImageFormat>
		class image : public details::features<TextureType> {
		  private:
			// a short alias of the base class, needed to easly refers to the exported features.
			using P = details::features<TextureType>;

		  public:
			using image_format = ImageFormat;
			using offset       = details::offset<P::dimensions>;
			using extent       = details::extent<P::dimensions>;
			using region       = details::region<P::dimensions>;
			using index        = details::image_index<P::has_mipmaps, P::has_array_layers, P::has_cubemap_faces>;

			// the class to allocate the storage for this image
			using allocator = details::storage<P::has_mipmaps, P::has_array_layers, P::has_cubemap_faces>;
			// the class to unpack from a client data
			using unpacker = details::unpacker<P::has_mipmaps, P::has_array_layers, P::has_cubemap_faces>;

			template <uint8_t SFINAE = P::dimensions, typename = std::enable_if_t<SFINAE == 1>>
			image(uint32_t width) {
				_sizes.width = width;
			}

			template <uint8_t SFINAE = P::dimensions, typename = std::enable_if_t<SFINAE == 2>>
			image(uint32_t width, uint32_t height) {
				_sizes.width  = width;
				_sizes.height = height;
			}

			template <uint8_t SFINAE = P::dimensions, typename = std::enable_if_t<SFINAE == 3>>
			image(uint32_t width, uint32_t height, uint32_t depth) {
				_sizes.width  = width;
				_sizes.height = height;
				_sizes.depth  = depth;
			}

			extent size() const {
				return _sizes;
			}

		  private:
			extent _sizes;
		};
	}
}
