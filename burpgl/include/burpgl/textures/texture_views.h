#pragma once
#include <glbinding/gl41core/enum.h>
#include <type_traits>

namespace burpgl {
	namespace textures {

		template <GL::GLenum Src, GL::GLenum... Dst>
		struct texture_compatibility : std::false_type {};

		// clang-format off
		template <>
		struct texture_compatibility<GL::GL_TEXTURE_1D, GL::GL_TEXTURE_1D> : std::true_type {};
		template <>
		struct texture_compatibility<GL::GL_TEXTURE_1D, GL::GL_TEXTURE_1D_ARRAY> : std::true_type {};

		template <>
		struct texture_compatibility<GL::GL_TEXTURE_2D, GL::GL_TEXTURE_2D> : std::true_type {};
		template <>
		struct texture_compatibility<GL::GL_TEXTURE_2D, GL::GL_TEXTURE_2D_ARRAY> : std::true_type {};

		template <>
		struct texture_compatibility<GL::GL_TEXTURE_3D, GL::GL_TEXTURE_3D> : std::true_type {};

		template <>
		struct texture_compatibility<GL::GL_TEXTURE_CUBE_MAP, GL::GL_TEXTURE_CUBE_MAP> : std::true_type {};
		template <>
		struct texture_compatibility<GL::GL_TEXTURE_CUBE_MAP, GL::GL_TEXTURE_2D> : std::true_type {};
		template <>
		struct texture_compatibility<GL::GL_TEXTURE_CUBE_MAP, GL::GL_TEXTURE_2D_ARRAY> : std::true_type {};
		template <>
		struct texture_compatibility<GL::GL_TEXTURE_CUBE_MAP, GL::GL_TEXTURE_CUBE_MAP_ARRAY> : std::true_type {};

		template <>
		struct texture_compatibility<GL::GL_TEXTURE_RECTANGLE, GL::GL_TEXTURE_RECTANGLE> : std::true_type {};

		template <>
		struct texture_compatibility<GL::GL_TEXTURE_1D_ARRAY, GL::GL_TEXTURE_1D> : std::true_type {};
		template <>
		struct texture_compatibility<GL::GL_TEXTURE_1D_ARRAY, GL::GL_TEXTURE_1D_ARRAY> : std::true_type {};

		template <>
		struct texture_compatibility<GL::GL_TEXTURE_2D_ARRAY, GL::GL_TEXTURE_2D> : std::true_type {};
		template <>
		struct texture_compatibility<GL::GL_TEXTURE_2D_ARRAY, GL::GL_TEXTURE_CUBE_MAP> : std::true_type {};
		template <>
		struct texture_compatibility<GL::GL_TEXTURE_2D_ARRAY, GL::GL_TEXTURE_2D_ARRAY> : std::true_type {};
		template <>
		struct texture_compatibility<GL::GL_TEXTURE_2D_ARRAY, GL::GL_TEXTURE_CUBE_MAP_ARRAY> : std::true_type {};

		template <>
		struct texture_compatibility<GL::GL_TEXTURE_CUBE_MAP_ARRAY, GL::GL_TEXTURE_CUBE_MAP> : std::true_type {};
		template <>
		struct texture_compatibility<GL::GL_TEXTURE_CUBE_MAP_ARRAY, GL::GL_TEXTURE_2D> : std::true_type {};
		template <>
		struct texture_compatibility<GL::GL_TEXTURE_CUBE_MAP_ARRAY, GL::GL_TEXTURE_2D_ARRAY> : std::true_type {};
		template <>
		struct texture_compatibility<GL::GL_TEXTURE_CUBE_MAP_ARRAY, GL::GL_TEXTURE_CUBE_MAP_ARRAY> : std::true_type {};

		template <>
		struct texture_compatibility<GL::GL_TEXTURE_2D_MULTISAMPLE, GL::GL_TEXTURE_2D_MULTISAMPLE> : std::true_type {};
		template <>
		struct texture_compatibility<GL::GL_TEXTURE_2D_MULTISAMPLE, GL::GL_TEXTURE_2D_MULTISAMPLE_ARRAY> : std::true_type {};

		template <>
		struct texture_compatibility<GL::GL_TEXTURE_2D_MULTISAMPLE_ARRAY, GL::GL_TEXTURE_2D_MULTISAMPLE> : std::true_type {};
		template <>
		struct texture_compatibility<GL::GL_TEXTURE_2D_MULTISAMPLE_ARRAY, GL::GL_TEXTURE_2D_MULTISAMPLE_ARRAY> : std::true_type {};
		// clang-format on
	}
}
