#pragma once
#include <cstdint>
#include <glbinding/gl41core/enum.h>

namespace burpgl {
	namespace textures {
		namespace details {
			/*
			 * `feature<>` exposes at compile time the features of the underlying image of the given texture
			 */
			// clang-format off
			template <
				GL::GLenum TextureType,
				bool Mipmaps,
				bool ArrayLayers,
				bool CubemapFaces,
				uint8_t Dimensions>
			struct features_ {
				constexpr static auto texture_type      = TextureType;
				constexpr static auto has_mipmaps       = Mipmaps;
				constexpr static auto has_array_layers  = ArrayLayers;
				constexpr static auto has_cubemap_faces = CubemapFaces;
				constexpr static auto dimensions        = Dimensions;
			};

			template<GL::GLenum TextureType>
			struct features;

			template<> struct features<GL::GL_TEXTURE_1D>                   : features_<GL::GL_TEXTURE_1D, true, false, false, 1>{};
			template<> struct features<GL::GL_TEXTURE_2D>                   : features_<GL::GL_TEXTURE_2D, true, false, false, 2>{};
			template<> struct features<GL::GL_TEXTURE_3D>                   : features_<GL::GL_TEXTURE_3D, true, false, false, 3>{};
			template<> struct features<GL::GL_TEXTURE_1D_ARRAY>             : features_<GL::GL_TEXTURE_1D_ARRAY, true, true, false, 1>{};
			template<> struct features<GL::GL_TEXTURE_2D_ARRAY>             : features_<GL::GL_TEXTURE_2D_ARRAY, true, true, false, 2>{};
			template<> struct features<GL::GL_TEXTURE_CUBE_MAP>             : features_<GL::GL_TEXTURE_CUBE_MAP, true, false, true, 2>{};
			template<> struct features<GL::GL_TEXTURE_CUBE_MAP_ARRAY>       : features_<GL::GL_TEXTURE_CUBE_MAP_ARRAY, true, true, true, 2>{};
			template<> struct features<GL::GL_TEXTURE_RECTANGLE>            : features_<GL::GL_TEXTURE_RECTANGLE, false, false, false, 2>{};
			template<> struct features<GL::GL_TEXTURE_BUFFER>               : features_<GL::GL_TEXTURE_BUFFER, false, false, false, 1>{};
			template<> struct features<GL::GL_TEXTURE_2D_MULTISAMPLE>       : features_<GL::GL_TEXTURE_2D_MULTISAMPLE, false, false, false, 2>{};
			template<> struct features<GL::GL_TEXTURE_2D_MULTISAMPLE_ARRAY> : features_<GL::GL_TEXTURE_2D_MULTISAMPLE_ARRAY, false, true, false, 2>{};
			// clang-format on
		}
	}
}
