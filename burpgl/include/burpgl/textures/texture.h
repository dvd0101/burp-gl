#pragma once
#include "../details/mpl.h"
#include "image.h"
#include "image_format.h"
#include "texture_views.h"
#include <glbinding/gl41core/functions.h>
#include <type_traits>

namespace burpgl {
	namespace textures {
		struct texture_handle : uint_handle {
			texture_handle() {
				GL::glGenTextures(1, &value);
				if (id() == 0) {
					throw handle_error{""};
				}
			}
			texture_handle(texture_handle&&) = default;

			~texture_handle() {
				if (id() != 0) {
					GL::glDeleteTextures(1, &value);
				}
			}
			texture_handle& operator=(texture_handle&&) = default;
		};

		namespace details {
			template <GL::GLenum TextureType, GL::GLenum ImageFormat>
			class texture_common {
			  public:
				using image_format = textures::format<ImageFormat>;
				using image_type   = textures::image<TextureType, image_format>;

				constexpr static auto texture_type = TextureType;

				template <typename SFINAE = image_type, typename = std::enable_if_t<SFINAE::dimensions == 1>>
				texture_common(uint32_t w) : _image{w} {}

				template <typename SFINAE = image_type, typename = std::enable_if_t<SFINAE::dimensions == 2>>
				texture_common(uint32_t w, uint32_t h) : _image{w, h} {}

				template <typename SFINAE = image_type, typename = std::enable_if_t<SFINAE::dimensions == 3>>
				texture_common(uint32_t w, uint32_t h, uint32_t d) : _image{w, h, d} {}

				template <typename T>
				void unpack(const T& dataset) {
					using I = typename image_type::index;
					unpack(I{}, dataset);
				}

				template <typename T>
				void unpack(typename image_type::index index, const T& dataset) {
					using Region = typename image_type::region;
					using Offset = typename image_type::offset;
					Region r{Offset{}, _image.size()};
					unpack(index, r, dataset);
				}

				template <typename T>
				void unpack(typename image_type::index index, typename image_type::region region, const T& dataset) {
					using Unpacker = typename image_type::unpacker;
					auto keep      = bind(*this);
					Unpacker::template unpack<TextureType>(index, region, dataset);
				}

				GL::GLuint id() const {
					return _handle;
				}

			  protected:
				void allocate(typename image_type::index ix) {
					using Allocator = typename image_type::allocator;
					auto keep       = bind(*this);
					Allocator::template allocate<TextureType, ImageFormat>(ix, _image.size());
				}

				template <GL::GLenum ViewTextureType, typename ViewImageFormat, typename... Args>
				void view(Args...) const {
					using TextureCompatibility = texture_compatibility<TextureType, ViewTextureType>;
					static_assert(TextureCompatibility::value,
					              "Cannot create a texture view, the new texture type is not compatible");

					static_assert(image_format::value == ViewImageFormat::value ||
					                  ViewImageFormat::compatibility != format_class::unknown,
					              "Cannot create a texture view, the new image format is not compatible");

					static_assert(image_format::value == ViewImageFormat::value ||
					                  image_format::compatibility == ViewImageFormat::compatibility,
					              "Cannot create a texture view, the two image formats are not compatible");
				}

				image_type _image;

			  private:
				texture_handle _handle;
			};

			template <GL::GLenum TextureType, GL::GLenum ImageFormat>
			auto bind(const texture_common<TextureType, ImageFormat>& t) {
				GL::glBindTexture(TextureType, t.id());
				return make_binder([]() { GL::glBindTexture(TextureType, 0); });
			}

			template <GL::GLenum TextureType, GL::GLenum ImageFormat, bool HasMipmaps, bool HasArrayLayers>
			class texture_impl;

			template <GL::GLenum TextureType, GL::GLenum ImageFormat>
			class texture_impl<TextureType, ImageFormat, true, true> : public texture_common<TextureType, ImageFormat> {
			  public:
				template <typename... Args>
				texture_impl(mipmap m, layer l, Args... args)
				    : texture_common<TextureType, ImageFormat>{args...}, _mipmap_levels{m}, _array_layers{l} {
					this->allocate({_mipmap_levels, _array_layers});
				}

				uint32_t levels() const {
					return _mipmap_levels;
				}

				uint32_t layers() const {
					return _array_layers;
				}

			  private:
				uint32_t _mipmap_levels;
				uint32_t _array_layers;
			};

			template <GL::GLenum TextureType, GL::GLenum ImageFormat>
			class texture_impl<TextureType, ImageFormat, true, false>
			    : public texture_common<TextureType, ImageFormat> {
			  public:
				template <typename... Args>
				texture_impl(mipmap m, Args... args)
				    : texture_common<TextureType, ImageFormat>{args...}, _mipmap_levels{m} {
					this->allocate({m});
				}

				uint32_t levels() const {
					return _mipmap_levels;
				}

			  private:
				uint32_t _mipmap_levels;
			};

			template <GL::GLenum TextureType, GL::GLenum ImageFormat>
			class texture_impl<TextureType, ImageFormat, false, true>
			    : public texture_common<TextureType, ImageFormat> {
			  public:
				template <typename... Args>
				texture_impl(layer l, Args... args)
				    : texture_common<TextureType, ImageFormat>{args...}, _array_layers{l} {
					this->allocate({_array_layers});
				}

				uint32_t layers() const {
					return _array_layers;
				}

			  private:
				uint32_t _array_layers;
			};

			template <GL::GLenum TextureType, GL::GLenum ImageFormat>
			class texture_impl<TextureType, ImageFormat, false, false>
			    : public texture_common<TextureType, ImageFormat> {
			  public:
				template <typename... Args>
				texture_impl(Args... args) : texture_common<TextureType, ImageFormat>{args...} {
					this->allocate({});
				}
			};

			template <GL::GLenum TextureType, GL::GLenum ImageFormat>
			using texture = texture_impl<TextureType, ImageFormat,
			                             textures::image<TextureType, textures::format<ImageFormat>>::has_mipmaps,
			                             textures::image<TextureType, textures::format<ImageFormat>>::has_array_layers>;
		}

		template <typename Name, GL::GLenum TextureType, GL::GLenum ImageFormat>
		class texture : public details::texture<TextureType, ImageFormat> {
		  public:
			using name = Name;
			using details::texture<TextureType, ImageFormat>::texture;
		};

		template <typename T>
		struct is_texture : std::false_type {};

		template <typename Name, GL::GLenum TextureType, GL::GLenum ImageFormat>
		struct is_texture<texture<Name, TextureType, ImageFormat>> : std::true_type {};
	}
}
