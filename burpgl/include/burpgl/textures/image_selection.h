#pragma once
#include "../details/mpl.h"
#include "../details/strong_number.h"
#include <boost/hana.hpp>

namespace burpgl {
	namespace textures {
		// a strong typedef that represents a number of mipmap levels
		using mipmap = strong_number<uint32_t, class _mipmap>;

		// a strong typedef that represents a number of array layers
		using layer = strong_number<uint32_t, class _layer>;

		// a strong typedef that represents a face of a cubemap texture
		using face = strong_number<uint8_t, class _face>;

		namespace faces {
			constexpr face positive_x = face{0};
			constexpr face negative_x = face{1};
			constexpr face positive_y = face{2};
			constexpr face negative_y = face{3};
			constexpr face positive_z = face{4};
			constexpr face negative_z = face{5};
		}

		namespace details {

			// An offset is used to represent the starting point of a sub-image
			// operation; there exists a specialization for every of the three
			// dimensions
			template <uint8_t Dimensions>
			struct offset;

			template <>
			struct offset<1> {
				uint32_t x = 0;
			};

			template <>
			struct offset<2> {
				uint32_t x = 0;
				uint32_t y = 0;
			};

			template <>
			struct offset<3> {
				uint32_t x = 0;
				uint32_t y = 0;
				uint32_t z = 0;
			};

			// An extent is used to represents the dimensions of a image; there
			// exists a specialization for every of the three dimensions
			template <uint8_t Dimensions>
			struct extent;

			template <>
			struct extent<1> {
				uint32_t width;

				uint32_t size() const {
					return width;
				}
			};

			template <>
			struct extent<2> {
				uint32_t width;
				uint32_t height;

				uint32_t size() const {
					return width * height;
				}
			};

			template <>
			struct extent<3> {
				uint32_t width;
				uint32_t height;
				uint32_t depth;

				uint32_t size() const {
					return width * height * depth;
				}
			};

			// A region is used to represents a subrange of a image
			// specialization for every of the three dimensions
			template <uint8_t Dimensions>
			struct region {
				offset<Dimensions> origin;
				extent<Dimensions> size;

				uint32_t area() const {
					return size.size();
				}
			};

			// An image_index is used to select an image inside a texture
			template <bool>
			struct image_index_mipmaps {};

			template <>
			struct image_index_mipmaps<true> {
				using type = mipmap;

				mipmap mipmap_level = mipmap{0};
			};

			template <bool>
			struct image_index_array {};

			template <>
			struct image_index_array<true> {
				using type = layer;

				layer array_layer = layer{0};
			};

			template <bool>
			struct image_index_cubemap {};

			template <>
			struct image_index_cubemap<true> {
				using type = face;

				face cubemap_face = face{0};
			};

			template <bool HasMipmaps, bool HasArrayLayers, bool HasCubemapFaces>
			struct image_index : image_index_mipmaps<HasMipmaps>,
			                     image_index_array<HasArrayLayers>,
			                     image_index_cubemap<HasCubemapFaces>
			/*
			 * `image_index` is used to select an image inside a texture.
			 *
			 * An image is identified using up to three attributes:
			 *  - mipmap level
			 *  - array layer
			 *  - face name
			 *
			 * how many of these attributes are actually needed depends on
			 * the specific texture type.
			 *
			 * The attributes are declared via three base classes; this way
			 * any attempt to misuse the index, maybe trying to set an
			 * attribute not supported by the texture, is caught at compile
			 * time.
			 *
			 * Incidentally, the usage of the base classes keeps the structure
			 * size to its minimum and, even if it is not the main goal, it is
			 * a good thing.
			 *
			 * You should never declare an `image_index<>` directly. Instead,
			 * use the type alias provided by the image class, which is
			 * already configured according to the texture type.
			 */
			{

				/*
				 * Some metaprogramming is required in order to provide a
				 * constructor that accepts a variable number of arguments
				 * that is compatible with the attributes of the index, and
				 * that does not require us to write an overload for every
				 * combination of the enabled attributes.
				 *
				 * Given the following function:
				 *
				 * 	void f(image_index<true, true, false>);
				 *
				 * we want to be able to call `f` as:
				 *
				 *  f({mipmap{1}, layer{0}});
				 */
			  private:
				// is_active is the predicate used to select the enabled bases;
				// declared here because it has no use outside this class
				template <typename>
				struct is_active;

				template <template <bool> class T, bool V>
				struct is_active<T<V>> {
					constexpr static auto value = V;
				};

				// bases is a typelist<> with the enabled subclasses
				using bases =
				    mpl::filter<is_active,
				                mpl::typelist<image_index_mipmaps<HasMipmaps>, image_index_array<HasArrayLayers>,
				                              image_index_cubemap<HasCubemapFaces>>>;

				template <typename... Args>
				constexpr static auto validate_constructor() {
					static_assert(mpl::length<bases> == sizeof...(Args),
					              "The constructor must be called with an argument for every index attribute");

					using namespace boost::hana;
					auto check = all_of(zip(
					                        // zip the type of the arguments passed to the constructor...
					                        tuple_t<Args...>,
					                        // ...with the type **associated** to the enabled bases: mipmap, layer or face.
					                        transform(mpl::to_hana<bases>,
					                                  [](auto base_type) {
						                                  // Base is one of the our bases
						                                  using Base = typename decltype(base_type)::type;
						                                  // Base::type is the associated type
						                                  return type_c<typename Base::type>;
						                              })),
					                    [](auto el) {
						                    // el is a "zipped" value, a tuple of two elements
						                    //  1. the "type" of the argument passed to the constructor
						                    //  2. the "type" of the attribute
						                    auto type1 = at_c<0>(el);
						                    auto type2 = at_c<1>(el);
						                    return type1 == type2;
						                });
					static_assert(
					    check, "The order or the types of the constructor arguments don't match the index attributes");
					return std::make_index_sequence<sizeof...(Args)>{};
				}

			  public:
				// default constructor; the resulting index point to the first image
				image_index() = default;

				// this constructor accepts an argument for every attribute of
				// the index; if the arguments are not valid, incompatible or
				// in the wrong order, a compile error is generated and a
				// diagnostic message emitted
				template <typename... Args>
				image_index(Args... args)
				    // here we are reusing the idea taken from
				    // http://stackoverflow.com/questions/42182335
				    // but adapted to use mpl::typelist instead of std::tuple
				    //
				    // Returning the integer_sequence from a function is not
				    // necessary; we could also call `make_index_sequence`
				    // directly, but a separate function is perfect to run our
				    // compile time checks about the arguments.
				    : image_index{validate_constructor<Args...>(), args...} {}

			  private:
				template <std::size_t... Is, typename... Args>
				image_index(std::index_sequence<Is...>, Args... args) : mpl::get<bases, Is>{args}... {}
			};
		}
	}
}
