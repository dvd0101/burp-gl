#pragma once
#include <glbinding/gl41core/enum.h>

namespace burpgl {
	namespace textures {

		enum class format_type { color, depth, stencil, depth_stencil };

		enum class format_class { bit128, bit96, bit64, bit48, bit32, bit24, bit16, bit8, unknown };

		namespace details {
			// clang-format off
			template <
				GL::GLenum Value,
				uint8_t Components,
				bool Normalized,
				uint8_t Bitdepth,
				bool BufferTexture,
				format_type ImageFormatKind,
				format_class FormatClass = format_class::unknown>
			struct format {
				constexpr static auto value   	     = Value;
				constexpr static auto components     = Components;
				constexpr static auto normalized     = Normalized;
				constexpr static auto bitdepth       = Bitdepth;
				constexpr static auto buffer_texture = BufferTexture;
				constexpr static auto kind           = ImageFormatKind;
				constexpr static auto compatibility  = FormatClass;
			};
			// clang-format on
		}

		template <GL::GLenum Value>
		struct format;

		// clang-format off
		template<> struct format<GL::GL_RGBA2>        : details::format<GL::GL_RGBA2,        4, true,   2, false, format_type::color>{};
		template<> struct format<GL::GL_RGBA4>        : details::format<GL::GL_RGBA4,        4, true,   4, false, format_type::color>{};
		template<> struct format<GL::GL_RGB4>         : details::format<GL::GL_RGB4,         3, true,   4, false, format_type::color>{};
		template<> struct format<GL::GL_RGB5>         : details::format<GL::GL_RGB5,         3, true,   5, false, format_type::color>{};
		template<> struct format<GL::GL_RGBA8>        : details::format<GL::GL_RGBA8,        4, true,   8,  true, format_type::color, format_class::bit32>{};
		template<> struct format<GL::GL_RGB8>         : details::format<GL::GL_RGB8,         3, true,   8, false, format_type::color, format_class::bit24>{};
		template<> struct format<GL::GL_RG8>          : details::format<GL::GL_RG8,          2, true,   8,  true, format_type::color, format_class::bit16>{};
		template<> struct format<GL::GL_R8>           : details::format<GL::GL_R8,           1, true,   8,  true, format_type::color, format_class::bit8>{};
		template<> struct format<GL::GL_RGBA8_SNORM>  : details::format<GL::GL_RGBA8_SNORM,  4, true,   8, false, format_type::color, format_class::bit32>{};
		template<> struct format<GL::GL_RGB8_SNORM>   : details::format<GL::GL_RGB8_SNORM,   3, true,   8, false, format_type::color, format_class::bit24>{};
		template<> struct format<GL::GL_RG8_SNORM>    : details::format<GL::GL_RG8_SNORM,    2, true,   8, false, format_type::color, format_class::bit16>{};
		template<> struct format<GL::GL_R8_SNORM>     : details::format<GL::GL_R8_SNORM,     1, true,   8, false, format_type::color, format_class::bit8>{};
		template<> struct format<GL::GL_RGB10>        : details::format<GL::GL_RGB10,        3, true,  10, false, format_type::color>{};
		template<> struct format<GL::GL_RGB12>        : details::format<GL::GL_RGB12,        3, true,  12, false, format_type::color>{};
		template<> struct format<GL::GL_RGBA12>       : details::format<GL::GL_RGBA12,       4, true,  12, false, format_type::color>{};
		template<> struct format<GL::GL_RGBA16>       : details::format<GL::GL_RGBA16,       4, true,  16,  true, format_type::color, format_class::bit64>{};
		template<> struct format<GL::GL_RGB16>        : details::format<GL::GL_RGB16,        3, true,  16, false, format_type::color, format_class::bit48>{};
		template<> struct format<GL::GL_RG16>         : details::format<GL::GL_RG16,         2, true,  16,  true, format_type::color, format_class::bit32>{};
		template<> struct format<GL::GL_R16>          : details::format<GL::GL_R16,          1, true,  16,  true, format_type::color, format_class::bit16>{};
		template<> struct format<GL::GL_RGBA16_SNORM> : details::format<GL::GL_RGBA16_SNORM, 4, true,  16, false, format_type::color, format_class::bit64>{};
		template<> struct format<GL::GL_RGB16_SNORM>  : details::format<GL::GL_RGB16_SNORM,  3, true,  16, false, format_type::color, format_class::bit48>{};
		template<> struct format<GL::GL_RG16_SNORM>   : details::format<GL::GL_RG16_SNORM,   2, true,  16, false, format_type::color, format_class::bit32>{};
		template<> struct format<GL::GL_R16_SNORM>    : details::format<GL::GL_R16_SNORM,    1, true,  16, false, format_type::color, format_class::bit16>{};
		template<> struct format<GL::GL_RGBA8UI>      : details::format<GL::GL_RGBA8UI,      4, false,  8,  true, format_type::color, format_class::bit32>{};
		template<> struct format<GL::GL_RGB8UI>       : details::format<GL::GL_RGB8UI,       3, false,  8, false, format_type::color, format_class::bit24>{};
		template<> struct format<GL::GL_RG8UI>        : details::format<GL::GL_RG8UI,        2, false,  8,  true, format_type::color, format_class::bit16>{};
		template<> struct format<GL::GL_R8UI>         : details::format<GL::GL_R8UI,         1, false,  8,  true, format_type::color, format_class::bit8>{};
		template<> struct format<GL::GL_RGBA8I>       : details::format<GL::GL_RGBA8I,       4, false,  8,  true, format_type::color, format_class::bit32>{};
		template<> struct format<GL::GL_RGB8I>        : details::format<GL::GL_RGB8I,        3, false,  8, false, format_type::color, format_class::bit24>{};
		template<> struct format<GL::GL_RG8I>         : details::format<GL::GL_RG8I,         2, false,  8,  true, format_type::color, format_class::bit16>{};
		template<> struct format<GL::GL_R8I>          : details::format<GL::GL_R8I,          1, false,  8,  true, format_type::color, format_class::bit8>{};
		template<> struct format<GL::GL_RGBA16UI>     : details::format<GL::GL_RGBA16UI,     4, false, 16,  true, format_type::color, format_class::bit64>{};
		template<> struct format<GL::GL_RGB16UI>      : details::format<GL::GL_RGB16UI,      3, false, 16, false, format_type::color, format_class::bit48>{};
		template<> struct format<GL::GL_RG16UI>       : details::format<GL::GL_RG16UI,       2, false, 16,  true, format_type::color, format_class::bit32>{};
		template<> struct format<GL::GL_R16UI>        : details::format<GL::GL_R16UI,        1, false, 16,  true, format_type::color, format_class::bit16>{};
		template<> struct format<GL::GL_RGBA16I>      : details::format<GL::GL_RGBA16I,      4, false, 16,  true, format_type::color, format_class::bit64>{};
		template<> struct format<GL::GL_RGB16I>       : details::format<GL::GL_RGB16I,       3, false, 16, false, format_type::color, format_class::bit48>{};
		template<> struct format<GL::GL_RG16I>        : details::format<GL::GL_RG16I,        2, false, 16,  true, format_type::color, format_class::bit32>{};
		template<> struct format<GL::GL_R16I>         : details::format<GL::GL_R16I,         1, false, 16,  true, format_type::color, format_class::bit16>{};
		template<> struct format<GL::GL_RGBA32UI>     : details::format<GL::GL_RGBA32UI,     4, false, 32,  true, format_type::color, format_class::bit128>{};
		template<> struct format<GL::GL_RGB32UI>      : details::format<GL::GL_RGB32UI,      3, false, 32,  true, format_type::color, format_class::bit96>{};
		template<> struct format<GL::GL_RG32UI>       : details::format<GL::GL_RG32UI,       2, false, 32,  true, format_type::color>{};
		template<> struct format<GL::GL_R32UI>        : details::format<GL::GL_R32UI,        1, false, 32,  true, format_type::color, format_class::bit32>{};
		template<> struct format<GL::GL_RGBA32I>      : details::format<GL::GL_RGBA32I,      4, false, 32,  true, format_type::color, format_class::bit128>{};
		template<> struct format<GL::GL_RGB32I>       : details::format<GL::GL_RGB32I,       3, false, 32,  true, format_type::color, format_class::bit96>{};
		template<> struct format<GL::GL_RG32I>        : details::format<GL::GL_RG32I,        2, false, 32,  true, format_type::color>{};
		template<> struct format<GL::GL_R32I>         : details::format<GL::GL_R32I,         1, false, 32,  true, format_type::color, format_class::bit32>{};
		template<> struct format<GL::GL_RGBA32F>      : details::format<GL::GL_RGBA32F,      4, false, 32,  true, format_type::color, format_class::bit128>{};
		template<> struct format<GL::GL_RGB32F>       : details::format<GL::GL_RGB32F,       3, false, 32,  true, format_type::color, format_class::bit96>{};
		template<> struct format<GL::GL_RG32F>        : details::format<GL::GL_RG32F,        2, false, 32,  true, format_type::color>{};
		template<> struct format<GL::GL_R32F>         : details::format<GL::GL_R32F,         1, false, 32,  true, format_type::color, format_class::bit32>{};
		template<> struct format<GL::GL_RGBA16F>      : details::format<GL::GL_RGBA16F,      4, false, 16,  true, format_type::color, format_class::bit64>{};
		template<> struct format<GL::GL_RGB16F>       : details::format<GL::GL_RGB16F,       3, false, 16, false, format_type::color, format_class::bit48>{};
		template<> struct format<GL::GL_RG16F>        : details::format<GL::GL_RG16F,        2, false, 16,  true, format_type::color, format_class::bit32>{};
		template<> struct format<GL::GL_R16F>         : details::format<GL::GL_R16F,         1, false, 16,  true, format_type::color, format_class::bit16>{};
		// clang-format on

		// clang-format off
		template<> struct format<GL::GL_DEPTH_COMPONENT16>  : details::format<GL::GL_DEPTH_COMPONENT16,  1, true,  16, false, format_type::depth>{};
		template<> struct format<GL::GL_DEPTH_COMPONENT24>  : details::format<GL::GL_DEPTH_COMPONENT24,  1, true,  24, false, format_type::depth>{};
		template<> struct format<GL::GL_DEPTH_COMPONENT32>  : details::format<GL::GL_DEPTH_COMPONENT32,  1, true,  32, false, format_type::depth>{};
		template<> struct format<GL::GL_DEPTH_COMPONENT32F> : details::format<GL::GL_DEPTH_COMPONENT32F, 1, false, 32, false, format_type::depth>{};
		// clang-format on
	}
}
