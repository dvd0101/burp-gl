#pragma once
#include "image_selection.h"

namespace burpgl {
	namespace textures {
		namespace details {
			template <bool HasMipmaps, bool HasArrayLayers, bool HasCubemapFaces>
			struct storage;

			template <bool HasMipmaps, bool HasArrayLayers, bool HasCubemapFaces>
			struct storage_base {
				using index = image_index<HasMipmaps, HasArrayLayers, HasCubemapFaces>;
			};

			template <>
			struct storage<true, true, true> : storage_base<true, true, true>
			// GL_TEXTURE_CUBE_MAP_ARRAY
			{

				template <GL::GLenum TextureType, GL::GLenum ImageFormat>
				static void allocate(index, extent<2>) {
					static_assert(TextureType == GL::GL_TEXTURE_CUBE_MAP_ARRAY, "Invalid/unsupported texture type");
					// glTexSubImage3D
				}
			};

			template <>
			struct storage<true, false, true> : storage_base<true, false, true>
			// GL_TEXTURE_CUBE_MAP
			{
				template <GL::GLenum TextureType, GL::GLenum ImageFormat>
				static void allocate(index, extent<2>) {
					static_assert(TextureType == GL::GL_TEXTURE_CUBE_MAP, "Invalid/unsupported texture type");
					// glTexSubImage2D
				}
			};

			template <>
			struct storage<true, true, false> : storage_base<true, true, false>
			// GL_TEXTURE_1D_ARRAY
			// GL_TEXTURE_2D_ARRAY
			{
				template <GL::GLenum TextureType, GL::GLenum ImageFormat>
				static void allocate(index, extent<1>) {
					// glTexSubImage2D
				}

				template <GL::GLenum TextureType, GL::GLenum ImageFormat>
				static void allocate(index, extent<2>) {
					static_assert(TextureType != GL::GL_TEXTURE_2D_MULTISAMPLE_ARRAY,
					              "Multisample textures cannot be updated from client data");
					// glTexSubImage3D
				}
			};

			template <>
			struct storage<true, false, false> : storage_base<true, false, false>
			// GL_TEXTURE_1D
			// GL_TEXTURE_2D
			// GL_TEXTURE_3D
			{
				template <GL::GLenum TextureType, GL::GLenum ImageFormat>
				static void allocate(index, extent<1>) {
					// glTexSubImage1D
				}

				template <GL::GLenum TextureType, GL::GLenum ImageFormat>
				static void allocate(index ix, extent<2> e) {
					static_assert(TextureType != GL::GL_TEXTURE_2D_MULTISAMPLE, "TODO: Multisample storage");
					GL::glTexStorage2D(TextureType, ix.mipmap_level, ImageFormat, e.width, e.height);
				}

				template <GL::GLenum TextureType, GL::GLenum ImageFormat>
				static void allocate(index, extent<3>) {
					// glTexSubImage3D
				}
			};

			template <>
			struct storage<false, false, false> : storage_base<false, false, false>
			// GL_TEXTURE_RECTANGLE
			// GL_TEXTURE_BUFFER
			{
				template <GL::GLenum TextureType, GL::GLenum ImageFormat>
				static void allocate(index, extent<1>) {
					// glTexSubImage1D
				}

				template <GL::GLenum TextureType, GL::GLenum ImageFormat>
				static void allocate(index, extent<2>) {
					// glTexSubImage2D
				}
			};
		}
	}
}
