#pragma once
#include <utility>

namespace burpgl {

	template <typename Unbind>
	class Binder {
	  public:
		Binder(Unbind&& f) noexcept : _f{std::move(f)}, _armed{true} {}

		Binder(Binder const&) = delete;

		Binder(Binder&& other) noexcept : _f{std::move(other._f)}, _armed{true} {
			other._armed = false;
		}

		~Binder() noexcept {
			if (_armed) {
				_f();
			}
		}

		Binder& operator=(Binder const&) = delete;

		Binder& operator=(Binder&& other) noexcept {
			_f           = std::move(other._f);
			other._armed = false;
			_armed       = true;
		}

	  private:
		Unbind _f;
		bool _armed;
	};

	template <typename Unbind>
	auto make_binder(Unbind&& f) {
		return Binder<Unbind>(std::forward<Unbind>(f));
	}
}
