#pragma once

#include <glbinding/gl45core/gl.h>

#if !defined BURPGL_VERTEX_ATTRIB_FORMAT
#if !defined(__APPLE__)
#define BURPGL_VERTEX_ATTRIB_FORMAT 1
#else
#define BURPGL_VERTEX_ATTRIB_FORMAT 0
#endif
#endif

#if !defined BURPGL_IMMUTABLE_BUFFER
#if !defined(__APPLE__)
#define BURPGL_IMMUTABLE_BUFFER 1
#else
#define BURPGL_IMMUTABLE_BUFFER 0
#endif
#endif

#if !defined BURPGL_IMMUTABLE_TEXTURE
#define BURPGL_IMMUTABLE_TEXTURE 1
#endif

namespace burpgl {
	const bool gl_vertex_attrib_format = BURPGL_IMMUTABLE_TEXTURE;
	const bool gl_immutable_buffer      = BURPGL_IMMUTABLE_BUFFER;
	const bool gl_immutable_texture     = BURPGL_IMMUTABLE_TEXTURE;

	namespace GL = gl45core;
}

#undef BURPGL_VERTEX_ATTRIB_FORMAT
#undef BURPGL_IMMUTABLE_BUFFER
#undef BURPGL_IMMUTABLE_TEXTURE
