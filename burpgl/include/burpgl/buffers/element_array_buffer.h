#pragma once
#include "./base.h"

namespace burpgl {
	namespace buffers {
		template <GL::GLenum T>
		constexpr bool check_element_array_type() {
			return T == GL::GL_UNSIGNED_BYTE || T == GL::GL_UNSIGNED_SHORT || T == GL::GL_UNSIGNED_INT;
		}

		template <GL::GLenum T>
		struct index_type;

		template <>
		struct index_type<GL::GL_UNSIGNED_BYTE> {
			using type = std::uint8_t;
		};

		template <>
		struct index_type<GL::GL_UNSIGNED_SHORT> {
			using type = std::uint16_t;
		};

		template <>
		struct index_type<GL::GL_UNSIGNED_INT> {
			using type = std::uint32_t;
		};
	}

	template <GL::GLbitfield StorageFlags, GL::GLenum IndexType>
	class element_array_buffer
	    : public buffers::base_buffer<GL::GL_ELEMENT_ARRAY_BUFFER, GL::GL_ELEMENT_ARRAY_BUFFER_BINDING, StorageFlags> {
	  private:
		using base =
		    buffers::base_buffer<GL::GL_ELEMENT_ARRAY_BUFFER, GL::GL_ELEMENT_ARRAY_BUFFER_BINDING, StorageFlags>;

	  public:
		static_assert(buffers::check_element_array_type<IndexType>(), "Invalid index type");
		constexpr static GL::GLenum index_type = IndexType;

		using type = typename buffers::index_type<IndexType>::type;

	  public:
		element_array_buffer(GL::GLsizeiptr elements) : base{gsl::narrow<GL::GLsizeiptr>(elements * sizeof(type))} {}
		element_array_buffer(gsl::span<const type> initial)
		    : base{gsl::span<const uint8_t>(reinterpret_cast<const uint8_t*>(initial.data()),
		                                    initial.size() * sizeof(type))} {}
	};

	template <typename>
	struct is_element_array_buffer : std::false_type {};

	template <auto... Attrs>
	struct is_element_array_buffer<element_array_buffer<Attrs...>> : std::true_type {};
}
