#pragma once
#include "../details/mpl.h"
#include "../gl_impl.h"
#include "exceptions.h"

namespace burpgl {

	template <auto... Args>
	void update(buffers::base_buffer<Args...>& buffer, GL::GLintptr offset, gsl::span<const uint8_t> source) {
		// clang-format off
		if constexpr(!buffer.features.dynamic_storage) {
			static_assert(
				mpl::always_false<void>,
				"update() uses glBufferSubData() and its usage requires the GL_DYNAMIC_STORAGE_BIT");
		}
		if (buffer.mapped() == map_mode::mapped_not_persistently) {
			throw buffer_mapped{};
		}
		// clang-format on

		const auto[buffer_offset, buffer_size] = range{offset, source.size()}.value(buffer);
		const auto keep = bind(buffer);
		GL::glBufferSubData(buffer.target_name, buffer_offset, buffer_size, source.data());
	}

	template <auto... Args>
	void clear(buffers::base_buffer<Args...>& buffer, uint8_t value, const range& r) {
		// clang-format off
		if (buffer.mapped() == map_mode::mapped_not_persistently) {
			throw buffer_mapped{};
		}
		// clang-format on

		const auto[offset, size] = r.value(buffer);
		const auto keep = bind(buffer);
		GL::glClearBufferSubData(buffer.target_name, GL::GL_R8, offset, size, GL::GL_RED, GL::GL_UNSIGNED_BYTE, &value);
	}

	template <auto... Args>
	void clear(buffers::base_buffer<Args...>& buffer, uint8_t value) {
		clear(buffer, value, {0, buffer.size()});
	}

	template <auto... ArgsSrc, auto... ArgsDst>
	void copy(const buffers::base_buffer<ArgsSrc...>& src, const range& src_range, buffers::base_buffer<ArgsDst...>& dst,
	          GL::GLintptr dst_offset) {
		// clang-format off
		if (dst.mapped() == map_mode::mapped_not_persistently) {
			throw buffer_mapped{};
		}
		if (src.mapped() == map_mode::mapped_not_persistently) {
			throw buffer_mapped{};
		}
		// clang-format on

		const auto[src_offset, size] = src_range.value(src);
		const range dst_range{dst_offset, size};
		dst_range.value(dst);

		if (src.id() == dst.id() && (src_range && dst_range)) {
			throw overlapped_range{};
		}

		GL::glBindBuffer(GL::GL_COPY_READ_BUFFER, src.id());
		GL::glBindBuffer(GL::GL_COPY_WRITE_BUFFER, dst.id());
		GL::glCopyBufferSubData(GL::GL_COPY_READ_BUFFER, GL::GL_COPY_WRITE_BUFFER, src_offset, dst_offset, size);
		GL::glBindBuffer(GL::GL_COPY_READ_BUFFER, 0);
		GL::glBindBuffer(GL::GL_COPY_WRITE_BUFFER, 0);
	}

	template <auto... ArgsSrc, auto... ArgsDst>
	void copy(const buffers::base_buffer<ArgsSrc...>& src, buffers::base_buffer<ArgsDst...>& dst, GL::GLintptr dst_offset = 0) {
		copy(src, {0, src.size()}, dst, dst_offset);
	}

	template <auto... Args, typename Container>
	void read(buffers::base_buffer<Args...>& buffer, GL::GLintptr offset, Container& container) {
		// clang-format off
		if (buffer.mapped() == map_mode::mapped_not_persistently) {
			throw buffer_mapped{};
		}
		// clang-format on
		const auto bytes = container.size() * sizeof(typename Container::value_type);
		const range r{offset, static_cast<GL::GLsizeiptr>(bytes)};
		r.value(buffer);

		auto ptr = static_cast<void*>(container.data());
		GL::glGetBufferSubData(buffer.target_name, offset, container.size(), ptr);
	}

	template <auto... Args, typename Container>
	void read(buffers::base_buffer<Args...>& buffer, Container& container) {
		read(buffer, 0, container);
	}
}
