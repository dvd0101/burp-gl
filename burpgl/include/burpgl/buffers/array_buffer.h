#pragma once
#include "../attributes.h"
#include "./base.h"

namespace burpgl {

	template <typename Attribute, size_t Offset>
	struct offset
	// an offset is used to describe at compile time where an attributes
	// starts in a buffer
	{
		using type = Attribute;
		static_assert(is_attribute<Attribute>::value, "type is not an attribute");

		constexpr static size_t value = Offset;
	};

	template <typename>
	struct is_offset : std::false_type {};

	template <typename T, size_t N>
	struct is_offset<offset<T, N>> : std::true_type {};

	namespace buffers {
		namespace h = boost::hana;
		struct strip_offset {
			template <typename T>
			constexpr auto operator()(h::basic_type<T> value) const {
				return value;
			}

			template <typename T, size_t N>
			constexpr auto operator()(h::basic_type<offset<T, N>>) const {
				return h::type_c<T>;
			}
		};

		template <typename... Attributes>
		constexpr auto strip_offsets() {
			auto attributes = h::transform(h::tuple_t<Attributes...>, strip_offset{});
			return h::unpack(attributes, h::template_<mpl::typelist>);
		}

		struct calc_offset {
			template <typename State, typename T, size_t Offset>
			constexpr auto operator()(State, hana::basic_type<offset<T, Offset>>) const {
				return hana::type_c<offset<T, Offset>>;
			}

			template <typename TState, size_t OffsetState, typename T>
			constexpr auto operator()(hana::basic_type<offset<TState, OffsetState>>, hana::basic_type<T>) const {
				return hana::type_c<offset<T, OffsetState + TState::size()>>;
			}

			template <typename T>
			constexpr auto operator()(hana::basic_type<void>, hana::basic_type<T>) const {
				return hana::type_c<offset<T, 0>>;
			}
		};

		template <typename... Attributes>
		constexpr auto calc_offsets() {
			auto offsets = h::drop_front(h::scan_left(h::tuple_t<Attributes...>, h::type_c<void>, calc_offset{}));
			return h::unpack(offsets, h::template_<mpl::typelist>);
		}

		template <typename Offsets>
		constexpr auto calc_stride() {
			auto last           = h::back(mpl::to_hana<Offsets>);
			using LastOffset    = typename decltype(last)::type;
			using LastAttribute = typename LastOffset::type;
			return LastAttribute::size() + LastOffset::value;
		}

		template <typename Attributes>
		struct search_offset {
			template <typename T>
			constexpr auto operator()(T value) const {
				using Offset = typename decltype(value)::type;
				return h::contains(mpl::to_hana<Attributes>, h::type_c<typename Offset::type>);
			}
		};

		template <typename Attributes, typename Offsets>
		constexpr auto search_offsets_for_attributes() {
			return h::filter(mpl::to_hana<Offsets>, search_offset<Attributes>{});
		}

		template <typename Attribute, typename Offsets>
		constexpr auto search_offset_for_attributes() {
			constexpr auto offsets = search_offsets_for_attributes<mpl::typelist<Attribute>, Offsets>();
			return offsets[hana::int_c<0>];
		}
	}

	template <GL::GLbitfield StorageFlags, typename... Attributes>
	class array_buffer : public buffers::base_buffer<GL::GL_ARRAY_BUFFER, GL::GL_ARRAY_BUFFER_BINDING, StorageFlags> {
	  public:
		using attributes = typename decltype(buffers::strip_offsets<Attributes...>())::type;
		static_assert(mpl::empty<mpl::remove<is_attribute, attributes>>, "type is not an attribute");

		using offsets = typename decltype(buffers::calc_offsets<Attributes...>())::type;

		inline constexpr static size_t stride = buffers::calc_stride<offsets>();

	  public:
		using buffers::base_buffer<GL::GL_ARRAY_BUFFER, GL::GL_ARRAY_BUFFER_BINDING, StorageFlags>::base_buffer;
	};

	template <typename>
	struct is_array_buffer : std::false_type {};

	template <GL::GLbitfield StorageFlags, typename... Attributes>
	struct is_array_buffer<array_buffer<StorageFlags, Attributes...>> : std::true_type {};
}
