#pragma once
#include <stdexcept>

namespace burpgl {
	struct range_error : std::logic_error {
		using logic_error::logic_error;
	};

	struct negative_offset : range_error {
		negative_offset() : range_error{"negative offset"} {}
	};

	struct negative_size : range_error {
		negative_size() : range_error{"negative size"} {}
	};

	struct zero_size : range_error {
		zero_size() : range_error{"size is zero"} {}
	};

	struct out_of_range : range_error {
		out_of_range() : range_error{"range exceeds the buffer size"} {}
	};

	struct overlapped_range : range_error {
		overlapped_range() : range_error{"ranges overlap"} {}
	};

	struct buffer_mapped : std::logic_error {
		buffer_mapped() : logic_error{"invalid operation when the buffer is mapped"} {}
	};

	struct map_error : std::logic_error {
		map_error() : logic_error{"buffer cannot be mapped (out of client memory?)"} {}
	};
}
