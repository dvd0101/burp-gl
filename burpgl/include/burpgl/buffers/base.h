#pragma once
#include "../3rdparty/gsl/gsl"
#include "../details/mpl.h"
#include "../bind.h"
#include "../gl_impl.h"
#include "../handle.h"
#include "exceptions.h"
#include <exception>
#include <tuple>

namespace burpgl {

	class range {
	  public:
		range(GL::GLintptr offset, GL::GLsizeiptr size) : _offset{offset}, _size{size} {
			if (offset < 0) {
				throw negative_offset{};
			}
			if (size < 0) {
				throw negative_size{};
			}
		}

		template <typename Buffer>
		std::tuple<GL::GLintptr, GL::GLsizeiptr> value(const Buffer& buffer) const {
			if (static_cast<size_t>(_offset) + _size > buffer.size()) {
				throw out_of_range{};
			}
			return std::make_tuple(_offset, _size);
		}

		bool operator&&(const range& b) const {
			size_t extent1 = static_cast<size_t>(_offset) + _size;
			size_t extent2 = static_cast<size_t>(b._offset) + b._size;
			return (b._offset > _offset && b._offset < extent1) || (_offset > b._offset && _offset < extent2);
		}

	  private:
		GL::GLintptr _offset;
		GL::GLsizeiptr _size;
	};

	enum class map_mode : uint8_t { unmapped = 0, mapped_not_persistently, mapped_persistently };

	namespace buffers {

		struct buffer_handle : uint_handle {
			buffer_handle() {
				GL::glGenBuffers(1, &value);
				if (id() == 0) {
					throw handle_error{""};
				}
			}
			buffer_handle(buffer_handle&&) = default;

			~buffer_handle() {
				if (id() != 0) {
					GL::glDeleteBuffers(1, &value);
				}
			}
			buffer_handle& operator=(buffer_handle&&) = default;
		};

		template <bool MapRead, bool MapWrite, bool MapPersistent, bool MapCoherent, bool DynamicStorage,
		          bool ClientStorage>
		struct storage_features {
			constexpr static bool map_read        = MapRead;
			constexpr static bool map_write       = MapWrite;
			constexpr static bool map_persistent  = MapPersistent;
			constexpr static bool map_coherent    = MapCoherent;
			constexpr static bool dynamic_storage = DynamicStorage;
			constexpr static bool client_storage  = ClientStorage;
		};

		template <GL::GLbitfield Flags>
		constexpr auto check_buffer_flags() {
			const auto full_bits = static_cast<GL::GLbitfield>(GL::BufferStorageMask::GL_NONE_BIT)              //
			                       | static_cast<GL::GLbitfield>(GL::BufferStorageMask::GL_MAP_READ_BIT)        //
			                       | static_cast<GL::GLbitfield>(GL::BufferStorageMask::GL_MAP_WRITE_BIT)       //
			                       | static_cast<GL::GLbitfield>(GL::BufferStorageMask::GL_MAP_PERSISTENT_BIT)  //
			                       | static_cast<GL::GLbitfield>(GL::BufferStorageMask::GL_MAP_COHERENT_BIT)    //
			                       | static_cast<GL::GLbitfield>(GL::BufferStorageMask::GL_DYNAMIC_STORAGE_BIT) //
			                       | static_cast<GL::GLbitfield>(GL::BufferStorageMask::GL_CLIENT_STORAGE_BIT); //
			static_assert((Flags | full_bits) == full_bits, "The flags contains an unknown value");

			const auto has_map_read  = Flags & static_cast<GL::GLbitfield>(GL::BufferStorageMask::GL_MAP_READ_BIT);
			const auto has_map_write = Flags & static_cast<GL::GLbitfield>(GL::BufferStorageMask::GL_MAP_WRITE_BIT);
			const auto has_persistent =
			    Flags & static_cast<GL::GLbitfield>(GL::BufferStorageMask::GL_MAP_PERSISTENT_BIT);

			static_assert(!has_persistent || (has_map_read || has_map_write),
			              "GL_MAP_PERSISTENT_BIT must be used "
			              "together with at least one of "
			              "GL_MAP_READ_BIT and GL_MAP_WRITE_BIT");

			const auto has_coherent = Flags & static_cast<GL::GLbitfield>(GL::BufferStorageMask::GL_MAP_COHERENT_BIT);
			static_assert(!has_coherent || has_persistent,
			              "GL_MAP_COHERENT_BIT must be used together with GL_MAP_PERSISTENT_BIT");

			const auto has_dynamic_storage =
			    Flags & static_cast<GL::GLbitfield>(GL::BufferStorageMask::GL_DYNAMIC_STORAGE_BIT);
			const auto has_client_storage =
			    Flags & static_cast<GL::GLbitfield>(GL::BufferStorageMask::GL_CLIENT_STORAGE_BIT);

			return storage_features<   //
			    has_map_read,          //
			    has_map_write,         //
			    has_persistent,        //
			    has_coherent,          //
			    has_dynamic_storage,   //
			    has_client_storage>{}; //
		}

		template <GL::GLbitfield StorageFlags, GL::GLbitfield AccessFlags>
		constexpr auto check_access_flags() {
			// clang-format off
			const auto has_read = StorageFlags & static_cast<GL::GLbitfield>(GL::BufferStorageMask::GL_MAP_READ_BIT);
			const auto want_read = AccessFlags & static_cast<GL::GLbitfield>(GL::BufferAccessMask::GL_MAP_READ_BIT);
			static_assert(!want_read || has_read,
			              "the buffer does not support the required mode (GL_MAP_READ_BIT)");

			const auto has_write = StorageFlags & static_cast<GL::GLbitfield>(GL::BufferStorageMask::GL_MAP_WRITE_BIT);
			const auto want_write = AccessFlags & static_cast<GL::GLbitfield>(GL::BufferAccessMask::GL_MAP_WRITE_BIT);
			static_assert(!want_write || has_write,
			              "the buffer does not support the required mode (GL_MAP_WRITE_BIT)");

			const auto has_persistent = StorageFlags & static_cast<GL::GLbitfield>(GL::BufferStorageMask::GL_MAP_PERSISTENT_BIT);
			const auto want_persistent = AccessFlags & static_cast<GL::GLbitfield>(GL::BufferAccessMask::GL_MAP_PERSISTENT_BIT);
			static_assert(!want_persistent || has_persistent,
			              "the buffer does not support the required mode (GL_MAP_PERSISTENT_BIT)");

			const auto has_coherent = StorageFlags & static_cast<GL::GLbitfield>(GL::BufferStorageMask::GL_MAP_COHERENT_BIT);
			const auto want_coherent = AccessFlags & static_cast<GL::GLbitfield>(GL::BufferAccessMask::GL_MAP_COHERENT_BIT);
			static_assert(!want_coherent || has_coherent,
			              "the buffer does not support the required mode (GL_MAP_COHERENT_BIT)");

			static_assert(want_read || want_write,
			              "neither GL_MAP_READ_BIT or GL_MAP_WRITE_BIT is set");
			const auto want_invalidate_range = AccessFlags & static_cast<GL::GLbitfield>(GL::BufferAccessMask::GL_MAP_INVALIDATE_RANGE_BIT);
			const auto want_invalidate_buffer = AccessFlags & static_cast<GL::GLbitfield>(GL::BufferAccessMask::GL_MAP_INVALIDATE_BUFFER_BIT);
			const auto want_unsynchronized = AccessFlags & static_cast<GL::GLbitfield>(GL::BufferAccessMask::GL_MAP_UNSYNCHRONIZED_BIT);
			const auto want_flush_explicit = AccessFlags & static_cast<GL::GLbitfield>(GL::BufferAccessMask::GL_MAP_FLUSH_EXPLICIT_BIT);
			// clang-format on

			static_assert(!want_read || !(want_invalidate_range || want_invalidate_buffer || want_unsynchronized),
			              "GL_MAP_READ_BIT can ot be used with one among GL_MAP_INVALIDATE_RANGE_BIT, "
			              "GL_MAP_INVALIDATE_BUFFER_BIT and GL_MAP_UNSYNCHRONIZED_BIT");
			static_assert(!want_flush_explicit || want_write,
			              "GL_MAP_FLUSH_EXPLICIT_BIT cannot be used without GL_MAP_WRITE_BIT");
		}

		template <typename Features>
		constexpr GL::GLenum derive_usage_hints(Features) {
			if (!Features::map_read && !Features::map_write) {
				if (Features::dynamic_storage) {
					return GL::GL_DYNAMIC_COPY;
				} else {
					return GL::GL_STATIC_COPY;
				}
			}
			if (!Features::map_read && Features::map_write) {
				return GL::GL_DYNAMIC_DRAW;
			}
			return GL::GL_DYNAMIC_READ;
		}

		template <GL::GLenum Target, GL::GLenum Pname, GL::GLbitfield Flags>
		class base_buffer {
		  public:
			constexpr static auto target_name = Target;
			constexpr static auto param_name  = Pname;

			static constexpr auto features = check_buffer_flags<Flags>();

			base_buffer(gsl::span<const uint8_t> initial)
			    : base_buffer(gsl::narrow<GL::GLsizeiptr>(initial.size()), initial.data()) {}

			base_buffer(GL::GLsizeiptr size) : base_buffer(size, nullptr) {}

			GL::GLuint id() const {
				return _handle;
			}

			static GL::GLuint current() {
				GL::GLuint id;
				GL::glGetIntegerv(param_name, reinterpret_cast<GL::GLint*>(&id));
				return id;
			}

			GL::GLsizeiptr size() const {
				return _size;
			}

		  public:
			class buffer_span : public gsl::span<uint8_t>
			/*
			 * A `buffer_span` is a `gls::span` over a region of the buffer
			 * mapped in the client memory, and it can only be constructed with
			 * a call to `base_buffer.map()`
			 *
			 * Since it is a `gsl::span` you can use all the facilities of the
			 * base class, with the additional guarantee that the buffer will
			 * be automatically unmapped once the `buffer_span` goes out of
			 * scope.
			 *
			 * You can also manually unmap the buffer using `unmap()`, it returns
			 * GL_FALSE if the memory has been trashed by the operating system
			 * and you need to reupload the buffer content.
			 *
			 * This is a rare and platform dependent event.
			 *
			 * If you have not explicitly called the `unmap()` it is invoked
			 * during the execution of the destructor; but at this point the
			 * caller cannot be safely informed about a memory corruption. So
			 * if `unmap()` returns GL_FALSE during the execution of the
			 * destructor, the program terminates.
			 *
			 * Quoting from:
			 * https://www.khronos.org/opengl/wiki/Buffer_Object#Buffer_Corruption
			 *
			 * > How often does this happen? On Microsoft Windows 5.1 (XP) and
			 * > below, video memory could get trashed anytime an application
			 * > didn't have input focus. This is why alt-tabbing away from games
			 * > takes a long time to recover from; the application/OpenGL has to
			 * > reload all of this data back to video memory. Fortunately, on
			 * > Windows 6.0 (Vista) and above, this is fixed; Windows itself
			 * > manages video memory and will ensure that all video memory is
			 * > retained. Thus, at least theoretically, this should never be a
			 * > problem on Vista or above machines.
			 */
			{
				friend class base_buffer;

			  public:
				~buffer_span() {
					if (_buffer->mapped() != map_mode::unmapped) {
						if (!unmap()) {
							std::terminate();
						}
					}
				}

				GL::GLboolean unmap() {
					const auto keep   = bind(*_buffer);
					const auto result = GL::glUnmapBuffer(_buffer->target_name);
					_buffer->_mapped  = map_mode::unmapped;
					return result;
				}

			  private:
				buffer_span(base_buffer* buffer, uint8_t* ptr, std::ptrdiff_t count)
				    : gsl::span<uint8_t>{ptr, count}, _buffer{buffer} {}

				base_buffer* _buffer;
			};

			map_mode mapped() const {
				return _mapped;
			}

			template <GL::GLbitfield AccessFlags>
			auto map(const range& r)
			/*
			 * Maps a buffer region in the client memory, it returns a
			 * `buffer_span` that can be used to read or write the memory
			 * according to `AccessFlags`.
			 */
			{
				check_access_flags<Flags, AccessFlags>();
				if (mapped() != map_mode::unmapped) {
					throw buffer_mapped{};
				}
				const auto[buffer_offset, buffer_size] = r.value(*this);
				if (buffer_size == 0) {
					throw zero_size{};
				}

				const auto keep = bind(*this);

				auto* ptr = glMapBufferRange(
				    target_name, buffer_offset, buffer_size, static_cast<GL::BufferAccessMask>(AccessFlags));
				if (!ptr) {
					throw map_error{};
				}
				const auto want_persistent =
				    AccessFlags & static_cast<GL::GLbitfield>(GL::BufferAccessMask::GL_MAP_PERSISTENT_BIT);
				_mapped = want_persistent ? map_mode::mapped_persistently : map_mode::mapped_not_persistently;
				return buffer_span(this, reinterpret_cast<uint8_t*>(ptr), buffer_size);
			};

			template <GL::GLbitfield AccessFlags>
			auto map() {
				return map<AccessFlags>({0, size()});
			}

			void invalidate(const range& r) {
				if (mapped() != map_mode::unmapped) {
					throw buffer_mapped{};
				}

				const auto[offset, size] = r.value(*this);

				// clang-format off
				if constexpr (gl_immutable_buffer) {
					GL::glInvalidateBufferSubData(id(), offset, size);
				} else {
					if (size == size()) {
						// When the entire buffer must be invalidated, reuse
						// the allocation function allows to complete the task
						// without the need to allocate client memory
						allocate_mutable_buffer(nullptr);
					} else {
						// This is suboptimal because the short-living map
						// needs to allocate some client memory only to throw
						// it away.
						constexpr auto map_flags = static_cast<GL::GLbitfield>(GL::BufferAccessMask::GL_MAP_WRITE_BIT)
							| static_cast<GL::GLbitfield>(GL::BufferAccessMask::GL_MAP_INVALIDATE_RANGE_BIT);
						map<map_flags>(r);
					}
				}
				// clang-format on
			}
			void invalidate() {
				invalidate({0, size()});
			}

		  private:
			base_buffer(GL::GLsizeiptr size, const void* ptr) : _size{size}, _mapped{map_mode::unmapped} {
				// clang-format off
				if constexpr (gl_immutable_buffer) {
					const auto keep = bind(*this);
					GL::glBufferStorage(target_name, _size, ptr, static_cast<GL::BufferStorageMask>(Flags));
				} else {
					allocate_mutable_buffer(ptr);
				}
				// clang-format on
			}

			void allocate_mutable_buffer(const void* ptr) {
				// clang-format off
				if constexpr (!gl_immutable_buffer) {
					const auto keep = bind(*this);
					constexpr auto hints = derive_usage_hints(features);
					GL::glBufferData(target_name, _size, ptr, hints);
				} else {
					static_assert(mpl::always_false<void>, "This function must not be called when the immutable buffers are available");
				}
				// clang-format on
			}

		  private:
			GL::GLsizeiptr _size;
			buffer_handle _handle;
			map_mode _mapped;
		};

		template <GL::GLenum Target, GL::GLenum Pname, GL::GLbitfield Flags>
		auto bind(const base_buffer<Target, Pname, Flags>& buff) {
			using BuffType = base_buffer<Target, Pname, Flags>;

			auto prev = BuffType::current();
			GL::glBindBuffer(BuffType::target_name, buff.id());
			return make_binder([prev]() { GL::glBindBuffer(BuffType::target_name, prev); });
		}
	}
}
