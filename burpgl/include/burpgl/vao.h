#pragma once
#include "./buffers/array_buffer.h"
#include "./buffers/element_array_buffer.h"
#include "bind.h"
#include "gl_impl.h"
#include "handle.h"
#include "vao_attributes_binding.h"
#include <algorithm>
#include <boost/hana.hpp>
#include <type_traits>

namespace burpgl {

	namespace hana = boost::hana;

	struct vertex_array_handle : uint_handle {
		vertex_array_handle() {
			GL::glGenVertexArrays(1, &value);
			if (id() == 0) {
				throw handle_error{""};
			}
		}

		vertex_array_handle(vertex_array_handle&&) = default;

		~vertex_array_handle() {
			if (id() != 0) {
				GL::glDeleteVertexArrays(1, &value);
			}
		}

		vertex_array_handle& operator=(vertex_array_handle&&) = default;
	};

	template <GL::GLenum Mode, typename... Attributes>
	class vao;

	template <GL::GLenum Mode, typename... Attributes>
	auto bind(const vao<Mode, Attributes...>& vao) {
		GL::glBindVertexArray(vao.id());
		return make_binder([]() { GL::glBindVertexArray(0); });
	}

	namespace details {
		constexpr bool valid_draw_mode(GL::GLenum mode) {
			return mode == GL::GL_POINTS || mode == GL::GL_LINE_STRIP || mode == GL::GL_LINE_LOOP ||
			       mode == GL::GL_LINES || mode == GL::GL_LINE_STRIP_ADJACENCY || mode == GL::GL_LINES_ADJACENCY ||
			       mode == GL::GL_TRIANGLE_STRIP || mode == GL::GL_TRIANGLE_FAN || mode == GL::GL_TRIANGLES ||
			       mode == GL::GL_TRIANGLE_STRIP_ADJACENCY || mode == GL::GL_TRIANGLES_ADJACENCY ||
			       mode == GL::GL_PATCHES;
		}

		template <typename...>
		struct merge_attributes;

		template <typename... ArrayBuffers>
		struct merge_attributes<mpl::typelist<ArrayBuffers...>> {
			using type = mpl::merge<typename ArrayBuffers::attributes...>;
		};

		template <typename>
		struct select_element_buffer;

		template <>
		struct select_element_buffer<mpl::typelist<>> {
			using type = void;
		};

		template <typename T>
		struct select_element_buffer<mpl::typelist<T>> {
			using type = T;
		};
	}

	template <GL::GLenum Mode, typename... Buffers>
	class vao {
	  public:
		using array_buffers = mpl::filter<is_array_buffer, mpl::typelist<Buffers...>>;
		static_assert(!mpl::empty<array_buffers>, "array_buffers are mandatory");

		using attributes = typename details::merge_attributes<array_buffers>::type;
		static_assert(std::is_same<mpl::unique<attributes>, attributes>::value,
		              "attribute provided by multiple buffers");

		constexpr static GL::GLenum mode = Mode;
		static_assert(details::valid_draw_mode(Mode), "invalid draw mode");

		static_assert(mpl::length<mpl::filter<is_element_array_buffer, mpl::typelist<Buffers...>>> <= 1,
		              "too many element buffers");
		using element_array_buffer = typename details::select_element_buffer<
		    mpl::filter<is_element_array_buffer, mpl::typelist<Buffers...>>>::type;

		vao() {
			// clang-format off
			if constexpr (gl_vertex_attrib_format) {
				bind_attributes_format();
			}
			// clang-format on
		}

		GL::GLuint id() const {
			return _handle;
		}

		template <typename Buffer>
		auto bind(const Buffer& buffer, GL::GLint offset = 0)
		    -> std::enable_if_t<mpl::contains<array_buffers, Buffer>> {

			auto keep = burpgl::bind(*this);

			// the local name is necessary only for OpenGL 4.3; but since it is
			// the index of the buffer it is also used to update the _size
			// attribute that keeps track of the element count  (and this is
			// necessary also when we use an older OpenGL)
			constexpr GL::GLuint binding_index = mpl::index_of<array_buffers, Buffer>;

			// clang-format off
			if constexpr (gl_vertex_attrib_format) {
				vertex_array_object::bind_source_buffer(binding_index, buffer.id(), offset, Buffer::stride);
			} else {
				auto keep_buffer = burpgl::buffers::bind(buffer);
				hana::for_each(mpl::to_hana<typename Buffer::offsets>, [&buffer, offset](auto off_type) {
					using Offset    = typename decltype(off_type)::type;
					using Attribute = typename Offset::type;

					vertex_array_object::bind_whole_attribute<Attribute>(buffer.stride, offset + Offset::value);
					// FIXME: no glDisableVertexAttribArray?
					GL::glEnableVertexAttribArray(Attribute::index);
				});
			}
			// clang-format on

			const size_t elements = buffer.size() / buffer.stride;
			_sizes[binding_index] = elements;
		}

		template <typename Buffer>
		auto bind(const Buffer&, GL::GLint = 0) -> std::enable_if_t<!mpl::contains<array_buffers, Buffer>> {
			static_assert(mpl::always_false<Buffer>, "unknown buffer type");
		}

		size_t vertices() const {
			return *std::min_element(begin(_sizes), end(_sizes));
		}

		template <GL::GLbitfield StorageFlags, GL::GLenum T, typename SFINAE = element_array_buffer>
		auto bind(const burpgl::element_array_buffer<StorageFlags, T>& buffer)
		    -> std::enable_if_t<!std::is_same<SFINAE, void>::value> {
			auto keep = burpgl::bind(*this);
			GL::glBindBuffer(GL::GL_ELEMENT_ARRAY_BUFFER, buffer.id());

			using IndexType = typename burpgl::element_array_buffer<StorageFlags, T>::type;
			_elements       = buffer.size() / sizeof(IndexType);
		}

		template <typename SFINAE = element_array_buffer>
		auto elements() const -> std::enable_if_t<!std::is_same<SFINAE, void>::value, uint32_t> {
			return _elements;
		}

	  private:
		void bind_attributes_format()
		// `bind_attributes_format` binds all the attributes provided by the buffers.
		//
		// Two bindings are performed:
		// - the attributes format
		// - the connection between the attributes and a buffer name
		//
		// The buffer name is an arbitrary integer, local to this vao, that
		// will associated (thanks to the `bind()` function) to the source of
		// the data (a buffer) for the connected attributes.
		// Example:
		// A VAO is configured to use two buffers, the first one (B1) provides
		// the data for two attributes (A and B) while the other (B2) for just
		// one (C).
		//
		// Attributes A and B will be bound to the same buffer name (N1), while
		// C to another one (N2).
		//
		// When the `bind()` function will be called, the buffer name
		// corresponding to the argument type (N1 for B1 or N2 for B2) will be
		// bound to the buffer id.
		{
			// clang-format off
			if constexpr (!gl_vertex_attrib_format) {
				static_assert(mpl::always_false<void>, "This method requires the vertex format api (OepnGL >= 4.3)");
			}
			// clang-format on
			auto keep = burpgl::bind(*this);

			hana::for_each(mpl::to_hana<array_buffers>, [](auto buff_type) {
				using Buffer = typename decltype(buff_type)::type;

				// For the local name of the buffer I use the position of the
				// type inside the buffers list.
				//
				// It is an integer good as an other; it is unique between the
				// buffers  and can be computed at compile time
				constexpr GL::GLuint binding_index = mpl::index_of<array_buffers, Buffer>;

				hana::for_each(mpl::to_hana<typename Buffer::attributes>, [binding_index](auto attr_type) {
					using Attribute = typename decltype(attr_type)::type;

					constexpr auto offset =
					    buffers::search_offset_for_attributes<Attribute, typename Buffer::offsets>();
					using Offset = typename decltype(offset)::type;

					vertex_array_object::bind_attribute_format<Attribute>(Offset::value);
					vertex_array_object::bind_attribute_buffer(Attribute::index, binding_index);
					GL::glEnableVertexAttribArray(Attribute::index);
				});
			});
		}

		vertex_array_handle _handle;
		std::array<size_t, mpl::length<array_buffers>> _sizes;
		uint32_t _elements = 0;
	};

	template <typename Vao>
	constexpr auto has_element_buffer =
	    std::conditional_t<std::is_same<typename Vao::element_array_buffer, void>::value, std::false_type,
	                       std::true_type>::value;
}
