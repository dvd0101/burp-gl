#pragma once
#include "3rdparty/gsl/gsl"
#include "details/concat.h"
#include "gl_impl.h"
#include "uniforms_programs.h"

namespace burpgl {
	/*
	 * uniform is used to describe the type of a variable passed to a shader via the Uniform* functions.
	 *
	 * Every uniform has a `name` known a compile time, a type and a
	 * cardinality (ie. every uniform can be viewed as an array of elements,
	 * with the array of size 1 as a special case).
	 *
	 * 	using mvp = uniform<STYPE("mvp"), GLfloat[4][4], 1>;
	 *
	 * 	`mvp` describes an uniform called "mvp" (and "mvp" is the name used at
	 * 	link time to retrieve the uniform location) which is *one* matrix of 4
	 * 	floats.
	 *
	 * 	using colors = uniform<STYPE("colors"), GLuint[3], 10>;
	 *
	 * 	`colors` is an array of 10 vectors made of three GLuint
	 *
	 * 	There are three kinds of uniforms:
	 *
	 *  +--------|------------------------+
	 *  | KIND   | Underlying type        |
	 *  +--------|------------------------+
	 *  | value  | GLfloat, GLint, GLuint |
	 *  |--------|------------------------|
	 *  | vector | GLfloat, GLint, GLuint |
	 *  |--------|------------------------|
	 *  | matrix | GLfloat                |
	 *  +---------------------------------+
	 */
	class uniform_index_error : public std::runtime_error {
	  public:
		// clang-format off
		uniform_index_error(size_t array_size, uint16_t index)
		    : runtime_error{concat(
					"uniform array index out of bounds;",
					" capacity=", array_size,
					" index=", index)} {}
		// clang-format on
	};

	class uniform_array_layout_error : public std::runtime_error {
	  public:
		// clang-format off
		uniform_array_layout_error(size_t element_size, size_t array_size)
		    : runtime_error{concat(
					"the value array is not evenly divisble; ",
					" element_size=", element_size,
					" array_size=", array_size)} {}
		// clang-format on
	};

	template <typename Name, typename UniformType, size_t Count>
	class uniform {
	  public:
		using name = Name;

		static_assert(uniforms::valid_uniform_type<UniformType>(), "uniform type not supported");
		using type = UniformType;

		static_assert(Count > 0, "An uniform cannot have zero elements");
		constexpr static bool is_array = (Count > 1);
		constexpr static auto elements = Count;

		uniform(GL::GLint l) : _location{l} {}

		template <bool IsArray = is_array>
		auto set(GL::GLuint prg, UniformType value) const -> std::enable_if_t<!IsArray>
		/*
		 * Changes the uniform value.
		 *
		 * This overload is available only if the uniform is not an array.
		 */
		{
			uniforms::set_uniform_vector<1>::set(prg, _location, &value, 1);
		}

		template <bool IsArray = is_array>
		auto set(GL::GLuint prg, gsl::span<const UniformType> value, uint16_t index = 0) const
		    -> std::enable_if_t<IsArray>
		/*
		 * Changes multiple values of the uniform array starting from `index`.
		 *
		 * The value can be anything that can be converted to a
		 * `span<UniformType>`, it's ok if it is larger than the uniform array
		 * because only `min(value.length, Count - index)` elements are
		 * transferred.
		 *
		 * An `uniform_index_error` is raised if the index is out of bound.
		 *
		 * A narrowing exception is raised if the index cannot be converted to
		 * the proper opengl type without losing information #TODO [api][practically impossible]
		 *
		 * This overload is available only if the uniform is an array.
		 */
		{
			if (index >= Count) {
				throw uniform_index_error(Count, index);
			}
			// XXX to future me, by specs it's ok to pass a `count` larger than
			// the array size, the exceeding values are ignored
			uniforms::set_uniform_vector<1>::set(prg,
			                                     gsl::narrow<GL::GLint>(_location + index),
			                                     value.data(),
			                                     gsl::narrow<GL::GLsizei>(value.size() - index));
		}

		template <bool IsArray = is_array>
		auto set(GL::GLuint prg, UniformType value, uint16_t index) const -> std::enable_if_t<IsArray>
		/*
		 * Changes the value of the uniform array at position `index`
		 *
		 * An `uniform_index_error` is raised if the index is out of bound.
		 *
		 * A narrowing exception is raised if the index cannot be converted to
		 * the proper opengl type without losing information #TODO [api][practically impossible]
		 *
		 * This overload is available only if the uniform is an array.
		 */
		{
			if (index >= Count) {
				throw uniform_index_error(Count, index);
			}
			uniforms::set_uniform_vector<1>::set(prg, gsl::narrow<GL::GLint>(_location + index), &value, 1);
		}

	  private:
		GL::GLint _location;
	};

	template <typename Name, typename UniformType, size_t VectorSize, size_t Count>
	class uniform<Name, UniformType[VectorSize], Count> {
	  public:
		using name = Name;

		static_assert(uniforms::valid_uniform_type<UniformType>(), "uniform type not supported");
		using type = UniformType;

		static_assert(Count > 0, "An uniform cannot have zero elements");
		constexpr static bool is_array = (Count > 1);
		constexpr static auto elements = Count;

		static_assert(VectorSize > 0 && VectorSize <= 4, "An uniform vector can have at most four elements");
		constexpr static uint8_t components = VectorSize;

		uniform(GL::GLint l) : _location{l} {}

		template <bool IsArray = is_array>
		auto set(GL::GLuint prg, gsl::span<const UniformType, VectorSize> value) const -> std::enable_if_t<!IsArray>
		/*
		 * Changes the uniform value.
		 * The value can be anything that can be converted to a
		 * `span<UniformType, VectorSize>`
		 *
		 * This overload is available only if the uniform is not an array.
		 */
		{
			uniforms::set_uniform_vector<VectorSize>::set(prg, _location, value.data(), 1);
		}

		template <bool IsArray = is_array>
		auto set(GL::GLuint prg, gsl::span<const UniformType> value, uint16_t index = 0) const
		    -> std::enable_if_t<IsArray>
		/*
		 * Changes multiple values of the uniform array starting from `index`.
		 *
		 * The value can be anything that can be converted to a
		 * `span<UniformType>`, it's ok if it is larger than the uniform array
		 * because only `min(value.length % VectorSize, Count - index)` elements are
		 * transferred.
		 *
		 * An `uniform_index_error` is raised if the index is out of bound.
		 *
		 * An `uniform_array_layout_error` is raised if the span cannot contain an even number of values.
		 *
		 * A narrowing exception is raised if the index cannot be converted to
		 * the proper opengl type without losing information #TODO [api][practically impossible]
		 *
		 * This overload is available only if the uniform is an array.
		 */
		{
			if (index >= Count) {
				throw uniform_index_error(Count, index);
			}
			auto count = value.size() / VectorSize;
			if (value.size() % VectorSize != 0) {
				throw uniform_array_layout_error(VectorSize, value.size());
			}
			uniforms::set_uniform_vector<VectorSize>::set(
			    prg, gsl::narrow<GL::GLint>(_location + index), value.data(), gsl::narrow<GL::GLsizei>(count));
		}

	  private:
		GL::GLint _location;
	};

	template <typename Name, typename UniformType, size_t Columns, size_t Rows, size_t Count>
	class uniform<Name, UniformType[Rows][Columns], Count> {
	  public:
		using name = Name;

		static_assert(std::is_same<UniformType, GL::GLfloat>::value, "uniform matrix type not supported");
		using type = UniformType;

		static_assert(Count > 0, "An uniform cannot have zero elements");
		constexpr static bool is_array = (Count > 1);
		constexpr static auto elements = Count;

		static_assert(Rows > 0 && Rows <= 4, "An uniform matrix can have at most four rows");
		constexpr static uint8_t rows = Rows;
		static_assert(Columns > 0 && Columns <= 4, "An uniform matrix can have at most four columns");
		constexpr static uint8_t columns = Columns;

		uniform(GL::GLint l) : _location{l} {}

		template <bool IsArray = is_array>
		auto set(GL::GLuint prg, gsl::span<const UniformType, Rows * Columns> value) const -> std::enable_if_t<!IsArray>
		/*
		 * Changes the uniform value.
		 * The value can be anything that can be converted to a
		 * `span<UniformType, VectorSize>`
		 *
		 * This overload is available only if the uniform is not an array.
		 */
		{
			uniforms::set_uniform_matrix<Columns, Rows>(prg, _location, value.data(), 1, uniforms::row_order);
		}

		template <bool IsArray = is_array>
		auto set(GL::GLuint prg, const UniformType value[Rows][Columns]) const -> std::enable_if_t<!IsArray>
		/*
		 * Changes the uniform value.
		 *
		 * This overload exists to make convenient to pass a static 2D array
		 *
		 * This overload is available only if the uniform is not an array.
		 */
		{
			uniforms::set_uniform_matrix<Columns, Rows>(
			    prg, _location, reinterpret_cast<const GL::GLfloat*>(value), 1, uniforms::row_order);
		}

		template <bool IsArray = is_array>
		auto set(GL::GLuint prg, gsl::span<const UniformType> value, uint16_t index = 0) const
		    -> std::enable_if_t<IsArray>
		/*
		 * Changes multiple matrices of the uniform array starting from `index`.
		 *
		 * The value can be anything that can be converted to a
		 * `span<UniformType>`, it's ok if it is larger than the uniform array
		 * because only `min(value.length % (Rows*Columns), Count - index)` elements are
		 * transferred.
		 *
		 * An `uniform_index_error` is raised if the index is out of bound.
		 *
		 * An `uniform_array_layout_error` is raised if the span cannot contain an even number of values.
		 *
		 * A narrowing exception is raised if the index cannot be converted to
		 * the proper opengl type without losing information #TODO [api][practically impossible]
		 *
		 * This overload is available only if the uniform is an array.
		 */
		{
			constexpr size_t MatrixSize = Rows * Columns;
			if (index >= Count) {
				throw uniform_index_error(Count, index);
			}

			auto count = value.size() / MatrixSize;
			if (value.size() % MatrixSize != 0) {
				throw uniform_array_layout_error(MatrixSize, value.size());
			}
			uniforms::set_uniform_matrix<Columns, Rows>(prg,
			                                            gsl::narrow<GL::GLint>(_location + index),
			                                            value.data(),
			                                            gsl::narrow<GL::GLsizei>(count),
			                                            uniforms::row_order);
		}

		template <bool IsArray = is_array>
		auto set(GL::GLuint prg, const UniformType value[Rows][Columns], uint16_t index = 0) const
		    -> std::enable_if_t<IsArray>
		/*
		 * Changes a single matrix in an uniform array;
		 *
		 * This convenience overload allow the use of a static 2D array;
		 *
		 * An `uniform_index_error` is raised if the index is out of bound.
		 *
		 * A narrowing exception is raised if the index cannot be converted to
		 * the proper opengl type without losing information #TODO [api][practically impossible]
		 *
		 * This overload is available only if the uniform is an array.
		 */
		{
			if (index >= Count) {
				throw uniform_index_error(Count, index);
			}

			uniforms::set_uniform_matrix<Columns, Rows>(prg,
			                                            gsl::narrow<GL::GLint>(_location + index),
			                                            reinterpret_cast<const GL::GLfloat*>(value),
			                                            1,
			                                            uniforms::row_order);
		}

	  private:
		GL::GLint _location;
	};

	template <typename>
	struct is_uniform : std::false_type {};

	template <typename Name, typename T, size_t count>
	struct is_uniform<uniform<Name, T, count>> : std::true_type {};
}
