#pragma once
#include "gl_impl.h"
#include "textures/texture.h"
#include "uniforms.h"

namespace burpgl {

	struct active_texture {
		GL::GLint texture_unit;
	};

	template <typename Name, typename TN, GL::GLenum TT, GL::GLenum TI, size_t Count>
	class uniform<Name, textures::texture<TN, TT, TI>, Count> : public uniform<Name, GL::GLint, Count> {
	  private:
		using P = uniform<Name, GL::GLint, Count>;

	  public:
		using texture_type = textures::texture<TN, TT, TI>;

		using uniform<Name, GL::GLint, Count>::uniform;

		template <bool IsArray = P::is_array>
		auto set(GL::GLuint prg, active_texture value) const -> std::enable_if_t<!IsArray> {
			P::set(prg, value.texture_unit);
		}

		template <bool IsArray = P::is_array>
		auto set(GL::GLuint prg, gsl::span<const active_texture> value, uint16_t index = 0) const
		    -> std::enable_if_t<IsArray> {
			// XXX ugly copy
			std::vector<GL::GLint> adapted(value.size());
			std::copy(std::begin(value), std::end(value), std::back_inserter(adapted));
			P::set(prg, adapted, index);
		}

		template <bool IsArray = P::is_array>
		auto set(GL::GLuint prg, active_texture value, uint16_t index) const -> std::enable_if_t<IsArray> {
			P::set(prg, value.texture_unit, index);
		}
	};

	template <typename>
	struct is_uniform_texture : std::false_type {};

	template <typename Name, typename TN, GL::GLenum TT, GL::GLenum TI, size_t Count>
	struct is_uniform_texture<uniform<Name, textures::texture<TN, TT, TI>, Count>> : std::true_type {};
}
