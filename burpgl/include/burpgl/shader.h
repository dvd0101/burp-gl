#pragma once
#include "attributes.h"
#include "details/mpl.h"
#include "gl_impl.h"
#include "handle.h"
#include "uniforms.h"
#include <boost/hana.hpp>
#include <stdexcept>
#include <string>
#include <vector>

namespace burpgl {
	namespace shaders {
		template <GL::GLenum ShaderType>
		struct shader_handle : uint_handle {
			shader_handle() : uint_handle{GL::glCreateShader(ShaderType)} {
				if (id() == 0) {
					throw handle_error{""};
				}
			}
			shader_handle(shader_handle&&) = default;

			// OpenGL keeps a ref counting of the used shaders, so it is safe to
			// destroy it even if it is used by something else (like a program)
			~shader_handle() {
				if (id() != 0) {
					GL::glDeleteShader(id());
				}
			}
			shader_handle& operator=(shader_handle&&) = default;
		};

		template <GL::GLenum ShaderType, typename... Params>
		class source {
		  public:
			constexpr static GL::GLenum type = ShaderType;

			using uniforms = mpl::filter<is_uniform, mpl::typelist<Params...>>;

		  public:
			void add(std::string src) {
				_sources.emplace_back(std::move(src));
			}

			const std::vector<std::string>& sources() const {
				return _sources;
			}

		  private:
			std::vector<std::string> _sources;
		};

		template <GL::GLenum ShaderType, typename Attributes, typename Uniforms>
		class compiled {
		  public:
			using attributes  = Attributes;
			using uniforms    = Uniforms;
			using handle_type = shader_handle<ShaderType>;

			constexpr static GL::GLenum type = ShaderType;

		  public:
			compiled(handle_type h) : _handle{std::move(h)} {}

			GL::GLuint id() const {
				return _handle;
			}

			std::string original_source() const {
				GL::GLint result;
				GL::glGetShaderiv(_handle, GL::GL_SHADER_SOURCE_LENGTH, &result);

				std::string buffer;
				if (result > 0) {
					buffer.resize(result);
					GL::glGetShaderSource(_handle, buffer.size(), nullptr, &buffer[0]);
				}
				return buffer;
			}

		  private:
			handle_type _handle;
		};

		template <GL::GLenum ShaderType, typename... Params>
		auto& operator<<(source<ShaderType, Params...>& sh, const std::string& src) {
			sh.add(src);
			return sh;
		}

		template <typename... Params>
		class vertex : public source<GL::GL_VERTEX_SHADER, Params...> {
		  public:
			using attributes = mpl::filter<is_attribute, mpl::typelist<Params...>>;
			static_assert(!mpl::empty<attributes>, "attributes are mandatory");
		};

		template <typename... Params>
		class geometry : public source<GL::GL_GEOMETRY_SHADER, Params...> {
		  public:
			using attributes = mpl::typelist<>;
		};

		template <typename... Params>
		class fragment : public source<GL::GL_FRAGMENT_SHADER, Params...> {
		  public:
			using attributes = mpl::typelist<>;
		};

		namespace details {

			template <GL::GLenum ShaderType>
			void replace_shader_source(shader_handle<ShaderType>& handler, const std::vector<std::string>& sources) {
				std::vector<const GL::GLchar*> ptrs(sources.size());
				std::vector<GL::GLint> lengths(sources.size());

				for (size_t index = 0; index < sources.size(); index++) {
					ptrs[index]    = sources[index].data();
					lengths[index] = sources[index].size();
				}

				GL::glShaderSource(handler, sources.size(), ptrs.data(), lengths.data());
			}
		}

		template <GL::GLenum ShaderType>
		std::string compile_log(shader_handle<ShaderType>& handler) {
			GL::GLint result;
			GL::glGetShaderiv(handler, GL::GL_INFO_LOG_LENGTH, &result);

			std::string buffer;
			if (result > 0) {
				buffer.resize(result);
				GL::glGetShaderInfoLog(handler, buffer.size(), nullptr, &buffer[0]);
			}
			return buffer;
		}

		class compile_error : public std::runtime_error {
		  public:
			compile_error(std::string log) : runtime_error{""}, _log{std::move(log)} {}

			const std::string& log() const {
				return _log;
			}

		  private:
			std::string _log;
		};

		template <typename S>
		auto compile(const S& sh) {
			using compiled      = compiled<S::type, typename S::attributes, typename S::uniforms>;
			using shader_handle = typename compiled::handle_type;

			shader_handle h;
			details::replace_shader_source(h, sh.sources());
			GL::glCompileShader(h);

			GL::GLint result;
			GL::glGetShaderiv(h, GL::GL_COMPILE_STATUS, &result);
			if (static_cast<GL::GLboolean>(result) != GL::GL_TRUE) {
				throw compile_error{compile_log(h)};
			}

			return compiled{std::move(h)};
		}
	}
}
