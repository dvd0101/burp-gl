#pragma once
#include "./details/strong_number.h"
#include <type_traits>

namespace burpgl {
	namespace uniforms {
		template <typename T>
		constexpr bool valid_uniform_type() {
			using std::is_same;
			return is_same<T, GL::GLfloat>::value   //
			       || is_same<T, GL::GLuint>::value //
			       || is_same<T, GL::GLint>::value; //
		}

		// set_uniform_vector is an implementation details used to remove the
		// differences between the variuos functions needed to program un
		// uniform vector
		template <size_t VectorSize>
		struct set_uniform_vector;

		template <>
		struct set_uniform_vector<1> {
			static void set(GL::GLuint prg, GL::GLint location, const GL::GLfloat* value, GL::GLsizei count) {
				GL::glProgramUniform1fv(prg, location, count, value);
			}

			static void set(GL::GLuint prg, GL::GLint location, const GL::GLint* value, GL::GLsizei count) {
				GL::glProgramUniform1iv(prg, location, count, value);
			}

			static void set(GL::GLuint prg, GL::GLint location, const GL::GLuint* value, GL::GLsizei count) {
				GL::glProgramUniform1uiv(prg, location, count, value);
			}
		};

		template <>
		struct set_uniform_vector<2> {
			static void set(GL::GLuint prg, GL::GLint location, const GL::GLfloat* value, GL::GLsizei count) {
				GL::glProgramUniform2fv(prg, location, count, value);
			}

			static void set(GL::GLuint prg, GL::GLint location, const GL::GLint* value, GL::GLsizei count) {
				GL::glProgramUniform2iv(prg, location, count, value);
			}

			static void set(GL::GLuint prg, GL::GLint location, const GL::GLuint* value, GL::GLsizei count) {
				GL::glProgramUniform2uiv(prg, location, count, value);
			}
		};

		template <>
		struct set_uniform_vector<3> {
			static void set(GL::GLuint prg, GL::GLint location, const GL::GLfloat* value, GL::GLsizei count) {
				GL::glProgramUniform3fv(prg, location, count, value);
			}

			static void set(GL::GLuint prg, GL::GLint location, const GL::GLint* value, GL::GLsizei count) {
				GL::glProgramUniform3iv(prg, location, count, value);
			}

			static void set(GL::GLuint prg, GL::GLint location, const GL::GLuint* value, GL::GLsizei count) {
				GL::glProgramUniform3uiv(prg, location, count, value);
			}
		};

		template <>
		struct set_uniform_vector<4> {
			static void set(GL::GLuint prg, GL::GLint location, const GL::GLfloat* value, GL::GLsizei count) {
				GL::glProgramUniform4fv(prg, location, count, value);
			}

			static void set(GL::GLuint prg, GL::GLint location, const GL::GLint* value, GL::GLsizei count) {
				GL::glProgramUniform4iv(prg, location, count, value);
			}

			static void set(GL::GLuint prg, GL::GLint location, const GL::GLuint* value, GL::GLsizei count) {
				GL::glProgramUniform4uiv(prg, location, count, value);
			}
		};

		// Just a meaningful name to the booleans used to describe if a given
		// matrix has column or row order
		using mx_order = strong_number<GL::GLboolean, class _mx_order>;
		const mx_order column_order{false};
		const mx_order row_order{true};

		// clang-format off

		// set_uniform_matrix is an implementation details used to remove the
		// differences between the variuos functions needed to program un
		// uniform matrix
		template <size_t Columns, size_t Rows>
		void set_uniform_matrix(GL::GLuint, GL::GLint, const GL::GLfloat*, GL::GLsizei, mx_order);

		template <>
		void set_uniform_matrix<2, 2>(GL::GLuint prg, GL::GLint location, const GL::GLfloat* value, GL::GLsizei count, mx_order order) {
			GL::glProgramUniformMatrix2fv(prg, location, count, order, value);
		}

		template <>
		void set_uniform_matrix<3, 3>(GL::GLuint prg, GL::GLint location, const GL::GLfloat* value, GL::GLsizei count, mx_order order) {
			GL::glProgramUniformMatrix3fv(prg, location, count, order, value);
		}

		template <>
		void set_uniform_matrix<4, 4>(GL::GLuint prg, GL::GLint location, const GL::GLfloat* value, GL::GLsizei count, mx_order order) {
			GL::glProgramUniformMatrix4fv(prg, location, count, order, value);
		}

		template <>
		void set_uniform_matrix<2, 3>(GL::GLuint prg, GL::GLint location, const GL::GLfloat* value, GL::GLsizei count, mx_order order) {
			GL::glProgramUniformMatrix2x3fv(prg, location, count, order, value);
		}

		template <>
		void set_uniform_matrix<3, 2>(GL::GLuint prg, GL::GLint location, const GL::GLfloat* value, GL::GLsizei count, mx_order order) {
			GL::glProgramUniformMatrix3x2fv(prg, location, count, order, value);
		}

		template <>
		void set_uniform_matrix<2, 4>(GL::GLuint prg, GL::GLint location, const GL::GLfloat* value, GL::GLsizei count, mx_order order) {
			GL::glProgramUniformMatrix2x4fv(prg, location, count, order, value);
		}

		template <>
		void set_uniform_matrix<4, 2>(GL::GLuint prg, GL::GLint location, const GL::GLfloat* value, GL::GLsizei count, mx_order order) {
			GL::glProgramUniformMatrix4x2fv(prg, location, count, order, value);
		}

		template <>
		void set_uniform_matrix<3, 4>(GL::GLuint prg, GL::GLint location, const GL::GLfloat* value, GL::GLsizei count, mx_order order) {
			GL::glProgramUniformMatrix3x4fv(prg, location, count, order, value);
		}

		template <>
		void set_uniform_matrix<4, 3>(GL::GLuint prg, GL::GLint location, const GL::GLfloat* value, GL::GLsizei count, mx_order order) {
			GL::glProgramUniformMatrix3x2fv(prg, location, count, order, value);
		}
		// clang-format on
	}
}
