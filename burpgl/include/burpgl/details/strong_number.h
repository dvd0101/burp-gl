#pragma once

namespace burpgl {
	template <typename T, typename Tag>
	struct strong_number {
	  public:
		explicit constexpr strong_number(T v) : value{v} {}

		constexpr bool operator==(strong_number o) const {
			return value == o.value;
		}

		constexpr bool operator!=(strong_number o) const {
			return value != o.value;
		}

		constexpr operator T() const {
			return value;
		}

	  private:
		T value;
	};
}
