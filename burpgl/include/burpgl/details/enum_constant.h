#pragma once
#include "../gl_impl.h"
#include <boost/hana.hpp>

namespace burpgl {
	template <GL::GLenum Value>
	using enum_constant = boost::hana::integral_constant<GL::GLenum, Value>;

	template <GL::GLenum Value>
	constexpr enum_constant<Value> enum_c{};
}

namespace boost { namespace hana {
    template<>
    struct hash_impl<integral_constant_tag<burpgl::GL::GLenum>, void> {
        template <typename V>
        static constexpr auto apply(V const& x) {
            return type_c<integral_constant<burpgl::GL::GLenum, V::value>>;
        }
    };
}}
