#pragma once
#include <boost/hana.hpp>
#include <type_traits>
#include <utility>

namespace burpgl {
	namespace mpl {
		template <typename... T>
		struct typelist {};

		template <typename T>
		constexpr bool empty = std::is_same<T, typelist<>>::value;

		template <typename T>
		constexpr auto always_false = false;

		template <typename List>
		struct length_;

		template <typename... T>
		struct length_<typelist<T...>> {
			constexpr static auto value = std::index_sequence_for<T...>::size();
		};

		template <typename List>
		constexpr auto length = length_<List>::value;

		template <typename List>
		struct head_;

		template <typename Head, typename... T>
		struct head_<typelist<Head, T...>> {
			using type = Head;
		};

		template <typename List>
		using head = typename head_<List>::type;

		template <typename T, typename... Args>
		struct cons_;

		template <typename T, typename... Args>
		struct cons_<T, typelist<Args...>> {
			using type = typelist<T, Args...>;
		};

		template <typename... Args1, typename... Args2>
		struct cons_<typelist<Args1...>, typelist<Args2...>> {
			using type = typelist<Args1..., Args2...>;
		};

		template <typename... Args>
		using cons = typename cons_<Args...>::type;

		template <template <typename> class Predicate, typename List>
		struct filter_;

		template <template <typename> class Predicate>
		struct filter_<Predicate, typelist<>> {
			using type = typelist<>;
		};

		template <template <typename> class Predicate, typename Head, typename... Tail>
		struct filter_<Predicate, typelist<Head, Tail...>> {
			using type =                                                              //
			    typename std::conditional<                                            //
			        Predicate<Head>::value,                                           //
			        cons<Head, typename filter_<Predicate, typelist<Tail...>>::type>, //
			        typename filter_<Predicate, typelist<Tail...>>::type              //
			        >::type;                                                          //
		};

		template <template <typename> class Predicate, typename List>
		using filter = typename filter_<Predicate, List>::type;

		template <template <typename> class Func, typename List>
		struct map_;

		template <template <typename> class Func>
		struct map_<Func, typelist<>> {
			using type = typelist<>;
		};

		template <template <typename> class Func, typename Head, typename... Tail>
		struct map_<Func, typelist<Head, Tail...>> {
			using type = cons<                                 //
			    typename Func<Head>::type,                     //
			    typename map_<Func, typelist<Tail...>>::type>; //
		};

		template <template <typename> class Func, typename List>
		using map = typename map_<Func, List>::type;

		template <typename List, typename T>
		struct contains_;

		template <typename T>
		struct contains_<typelist<>, T> : std::false_type {};

		template <typename Head, typename... Tail, typename T>
		struct contains_<typelist<Head, Tail...>, T> {
			constexpr static auto value = contains_<typelist<Tail...>, T>::value;
		};

		template <typename... Tail, typename T>
		struct contains_<typelist<T, Tail...>, T> : std::true_type {};

		template <typename List, typename T>
		constexpr auto contains = contains_<List, T>::value;

		template <template <typename> class Predicate, typename T>
		struct negate_ {
			constexpr static bool value = !Predicate<T>::value;
		};

		template <template <typename> class Predicate, typename List>
		struct remove_ {
			template <typename T>
			using not_p = negate_<Predicate, T>;

			using type = filter<not_p, List>;
		};

		template <template <typename> class Predicate, typename List>
		using remove = typename remove_<Predicate, List>::type;

		template <typename T, typename U>
		struct is_not_same : std::true_type {};

		template <typename T>
		struct is_not_same<T, T> : std::false_type {};

		template <typename List>
		struct unique_;

		template <>
		struct unique_<typelist<>> {
			using type = typelist<>;
		};

		template <typename T>
		struct unique_<typelist<T>> {
			using type = typelist<T>;
		};

		template <typename Head, typename... Tail>
		struct unique_<typelist<Head, Tail...>> {
			template <typename P>
			using Predicate = is_not_same<Head, P>;

			using type = cons<Head, typename unique_<filter<Predicate, typelist<Tail...>>>::type>;
		};

		template <typename List>
		using unique = typename unique_<List>::type;

		template <typename... Lists>
		struct merge_;

		template <>
		struct merge_<> {
			using type = typelist<>;
		};

		template <typename List>
		struct merge_<List> {
			using type = List;
		};

		template <typename... Args1, typename... Args2, typename... Other>
		struct merge_<typelist<Args1...>, typelist<Args2...>, Other...> {
			using type = typename merge_<typelist<Args1..., Args2...>, Other...>::type;
		};

		template <typename... Lists>
		using merge = typename merge_<Lists...>::type;

		template <typename List, typename T, int pos>
		struct index_of_;

		template <typename T, int pos>
		struct index_of_<typelist<>, T, pos> {
			static_assert(always_false<T>, "value not found");
		};

		template <typename Head, typename... Tail, typename T, int pos>
		struct index_of_<typelist<Head, Tail...>, T, pos> {
			enum { value = index_of_<typelist<Tail...>, T, pos + 1>::value };
		};

		template <typename T, typename... Tail, int pos>
		struct index_of_<typelist<T, Tail...>, T, pos> {
			enum { value = pos };
		};

		template <typename List, typename T>
		constexpr auto index_of = index_of_<List, T, 0>::value;

		struct get__not_found;

		template <typename List, int Target, int CurrentIndex>
		struct get__search;

		template <int Target, int CurrentIndex>
		struct get__search<typelist<>, Target, CurrentIndex> {
			using type = get__not_found;
		};

		template <typename T, typename... Tail, int Target, int CurrentIndex>
		struct get__search<typelist<T, Tail...>, Target, CurrentIndex> {
			using type = typename std::conditional<                                     //
			    Target == CurrentIndex,                                                 //
			    T,                                                                      //
			    typename get__search<typelist<Tail...>, Target, CurrentIndex + 1>::type //
			    >::type;                                                                //
		};

		template <typename List, int Target>
		struct get_ {
			using type = typename get__search<List, Target, 0>::type;
			static_assert(!std::is_same<type, get__not_found>::value, "index not found");
		};

		template <typename List, int Index>
		using get = typename get_<List, Index>::type;

		template <typename List>
		struct to_hana_;

		template <typename... T>
		struct to_hana_<typelist<T...>> {
			constexpr static auto value = boost::hana::tuple_t<T...>;
		};

		template <typename List>
		constexpr auto to_hana = to_hana_<List>::value;

		template <typename Sequence>
		constexpr auto from_hana(Sequence& seq) {
			return boost::hana::unpack(seq, boost::hana::template_<mpl::typelist>);
		}

		template <typename List>
		struct to_tuple_;

		template <typename... T>
		struct to_tuple_<typelist<T...>> {
			using type = std::tuple<T...>;
		};

		template <typename List>
		using to_tuple = typename to_tuple_<List>::type;
	}
}
