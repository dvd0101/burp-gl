#pragma once
#include <boost/hana.hpp>
namespace burpgl {

	namespace hana = boost::hana;

	template <typename Iterable, typename T>
	constexpr auto index_of(Iterable const& iterable, T const& element)
	// copied from http://stackoverflow.com/a/33987589/981321
	{
		auto size    = decltype(hana::size(iterable)){};
		auto dropped = decltype(hana::size(hana::drop_while(iterable, hana::not_equal.to(element)))){};
		return size - dropped;
	}
}
