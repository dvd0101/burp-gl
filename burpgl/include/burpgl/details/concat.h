#pragma once
#include <sstream>

namespace burpgl {
	template <typename... Args>
	std::string concat(Args&&... args) {
		std::ostringstream s;
		// courtesy of: http://en.cppreference.com/w/cpp/language/parameter_pack
		using _ = int[];
		(void)_{(s << args, 0)...};
		return s.str();
	}
}
