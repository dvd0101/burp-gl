#pragma once
#include "gl_impl.h"
#include "program.h"
#include "vao.h"
#include <boost/hana.hpp>
#include <functional>
#include <type_traits>

namespace burpgl {
	namespace hana = boost::hana;

	namespace draw {
		namespace details {
			template <typename Uniforms, typename Texture>
			struct find_texture_impl {
				template <typename H>
				constexpr auto operator()(H uniform_type) const {
					using Uniform        = typename decltype(uniform_type)::type;
					using UniformTexture = typename Uniform::texture_type;
					return hana::bool_c<std::is_same<UniformTexture, Texture>::value>;
				}
			};
			template <typename Uniforms, typename Texture>
			constexpr auto find_texture() {
				auto result = hana::find_if(mpl::to_hana<Uniforms>, find_texture_impl<Uniforms, Texture>{});
				static_assert(result != hana::nothing, "the texture does not exist in the program");
				return result.value();
			}

			namespace calc_texture_unit_impl {
				struct fold_function {
					template <typename H>
					constexpr auto operator()(int acc, H uniform_type) const {
						using T = typename decltype(uniform_type)::type;
						return acc + T::elements;
					}
				};

				template <typename TextureUniform>
				struct take_while_function {
					template <typename H>
					constexpr auto operator()(H uniform_type) const {
						using T = typename decltype(uniform_type)::type;
						return hana::bool_c<!std::is_same<TextureUniform, T>::value>;
					}
				};

				template <typename Uniforms, typename TextureUniform>
				constexpr auto calc_texture_unit() {
					// clang-format off
					return hana::fold(
						hana::take_while(
							mpl::to_hana<Uniforms>,
							take_while_function<TextureUniform>{}),
						0,
						fold_function{}
					);
					// clang-format on
				}
			}
			using calc_texture_unit_impl::calc_texture_unit;
		}

		template <typename Prg, typename Vao>
		class context_ {
		  public:
			context_(Prg& p, Vao& v) : _prg{p}, _vao{v}, _keep_prg{bind(p)}, _keep_vao{bind(v)} {}

			auto program() const -> const Prg& {
				return _prg;
			}

			auto program() -> std::enable_if_t<!std::is_const<Prg>::value, Prg&> {
				return _prg;
			}

			auto vao() const -> const Vao& {
				return _vao;
			}

			auto vao() -> std::enable_if_t<!std::is_const<Vao>::value, Vao&> {
				return _vao;
			}

			template <typename Texture>
			void texture(const Texture& t) {
				texture(t, 0);
			}

			template <typename Texture>
			void texture(const Texture& t, uint16_t index) {
				using Textures = typename Prg::textures;
				// Wrapped is an hana::type_c with the Uniform linked to the Texture
				constexpr auto Wrapped = details::find_texture<Textures, Texture>();

				using Uniform = typename decltype(Wrapped)::type;

				constexpr GL::GLuint first_unit = details::calc_texture_unit<Textures, Uniform>();

				const GL::GLint texture_unit = first_unit + index;
				GL::glActiveTexture(GL::GL_TEXTURE0 + texture_unit);
				GL::glBindTexture(Texture::texture_type, t.id());
				_prg.get().template set_uniform<Uniform>(active_texture{texture_unit}, index);
			}

		  public:
			void draw_array(GL::GLint first = 0) const {
				draw_array(first, _vao.get().vertices());
			}

			void draw_array(GL::GLint first, GL::GLsizei count) const {
				GL::glDrawArrays(Vao::mode, first, count);
			}

			void draw_array_instanced(GL::GLsizei instances, GL::GLint first = 0) const {
				draw_array_instanced(instances, first, _vao.get().vertices());
			}

			void draw_array_instanced(GL::GLsizei instances, GL::GLint first, GL::GLsizei count) const {
				GL::glDrawArraysInstanced(Vao::mode, first, count, instances);
			}

		  public:
			template <typename VAO = Vao>
			auto draw_elements() -> std::enable_if_t<has_element_buffer<VAO>> {
				draw_elements(0, _vao.get().elements());
			}

			template <typename VAO = Vao>
			auto draw_elements(uint32_t offset, GL::GLsizei count) -> std::enable_if_t<has_element_buffer<VAO>> {
				auto ptr = reinterpret_cast<const GL::GLvoid*>(offset);
				GL::glDrawElements(Vao::mode, count, Vao::element_array_buffer::index_type, ptr);
			}

		  private:
			std::reference_wrapper<Prg> _prg;
			std::reference_wrapper<Vao> _vao;

			decltype(bind(_prg.get())) _keep_prg;
			decltype(bind(_vao.get())) _keep_vao;
		};

		template <typename Prg, typename Vao>
		auto context(Prg& prg, Vao& vao) {
			using ProgramAttributes = typename Prg::attributes;
			using VaoAttributes     = typename Vao::attributes;
			// clang-format off
			constexpr auto diff = hana::difference(
				hana::to_set(mpl::to_hana<ProgramAttributes>),
				hana::to_set(mpl::to_hana<VaoAttributes>)
			);
			static_assert(
				hana::length(diff) == hana::size_c<0>,
				"The VAO does not provide all the needed attributes");
			// clang-format on
			return context_<Prg, Vao>{prg, vao};
		}
	}
}
