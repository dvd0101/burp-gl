#pragma once
#include "bind.h"
#include "details/concat.h"
#include "details/enum_constant.h"
#include "details/hana_ext.h"
#include "details/mpl.h"
#include "gl_impl.h"
#include "handle.h"
#include "shader.h"
#include "uniforms_samplers.h"
#include <boost/hana.hpp>
#include <boost/hana/ext/std/tuple.hpp>
#include <stdexcept>
#include <tuple>

namespace burpgl {
	namespace shaders {

		struct program_handle : uint_handle {
			program_handle() : uint_handle{GL::glCreateProgram()} {
				if (id() == 0) {
					throw handle_error{""};
				}
			}
			program_handle(program_handle&&) = default;

			// OpenGL keeps a ref counting of the used shaders, so it is safe to
			// destroy it even if it is used by something else (like a program)
			~program_handle() {
				if (id() != 0) {
					GL::glDeleteProgram(id());
				}
			}
			program_handle& operator=(program_handle&&) = default;
		};

		class uniform_location_error : public std::runtime_error {
		  public:
			uniform_location_error(const std::string& name) : runtime_error{concat("uniform `", name, "` not found")} {}
		};

		namespace details {
			template <typename... Uniforms>
			auto load_uniforms(const program_handle& handle) {
				constexpr auto types = hana::tuple_t<Uniforms...>;

				auto output = hana::transform(types, [&handle](auto value) {
					using T = typename decltype(value)::type;

					GL::GLint loc = GL::glGetUniformLocation(handle, T::name::data());
					if (loc == -1) {
						throw uniform_location_error{T::name::data()};
					}
					return T{loc};
				});
				return hana::to<hana::ext::std::tuple_tag>(output);
			}
		}

		template <typename Attributes, typename Uniforms, typename Textures>
		class program;

		template <typename... Attribute, typename... Uniform, typename... Texture>
		class program<mpl::typelist<Attribute...>, mpl::typelist<Uniform...>, mpl::typelist<Texture...>> {
		  public:
			using attributes = mpl::typelist<Attribute...>;
			using uniforms   = mpl::typelist<Uniform...>;
			using textures   = mpl::typelist<Texture...>;

		  public:
			program(program_handle h) : _handle{std::move(h)}, _uniforms{details::load_uniforms<Uniform...>(_handle)} {}

			GL::GLuint id() const {
				return _handle;
			}

			template <typename U, typename... Args>
			void set_uniform(Args&&... args) {
				static_assert(mpl::contains<uniforms, U>, "unknown uniform");
				constexpr auto index = index_of(hana::tuple_t<Uniform...>, hana::type_c<U>);

				auto& uniform = std::get<index>(_uniforms);
				uniform.set(_handle, std::forward<Args>(args)...);
			}

		  private:
			program_handle _handle;

			std::tuple<Uniform...> _uniforms;
		};

		template <typename... T>
		auto bind(const program<T...>& prg) {
			GL::glUseProgram(prg.id());
			return make_binder([]() { GL::glUseProgram(0); });
		}

		namespace details {
			template <typename... Shader>
			constexpr bool has_duplicate_shaders() {
				namespace h = boost::hana;

				auto types  = h::make_tuple(enum_c<Shader::type>...);
				auto unique = h::to_tuple(h::to_set(types));
				return h::length(types) != h::length(unique);
			}

			template <GL::GLenum GLShaderType, typename... Shader>
			constexpr bool has_shader_type() {
				namespace h = boost::hana;

				auto shader_types = h::make_tuple(enum_c<Shader::type>...);
				return h::contains(shader_types, enum_c<GLShaderType>);
			}

			template <GL::GLenum GLShaderType, typename... Shader>
			constexpr auto get_shader_by_type() {
				namespace h = boost::hana;
				return h::find_if(                                          //
				    h::make_tuple(h::type_c<Shader>...),                    //
				    [](auto&& sh) {                                         //
					    using ShaderType = typename decltype(+sh)::type;    //
					    return h::bool_c<ShaderType::type == GLShaderType>; //
					}                                                       //
				    );                                                      //
			}
		}

		std::string link_log(program_handle& handle) {
			GL::GLint result;
			GL::glGetProgramiv(handle, GL::GL_INFO_LOG_LENGTH, &result);

			std::string buffer;
			if (result > 0) {
				buffer.resize(result);
				GL::glGetProgramInfoLog(handle, buffer.size(), nullptr, &buffer[0]);
			}
			return buffer;
		}

		class attach_error : public std::runtime_error {
		  public:
			attach_error() : runtime_error{"shader attach error"} {}
		};

		class link_error : public std::runtime_error {
		  public:
			link_error(std::string log) : runtime_error{""}, _log{std::move(log)} {}

			const std::string& log() const {
				return _log;
			}

		  private:
			std::string _log;
		};

		template <typename... Compiled>
		auto link(const Compiled&... shaders) {
			static_assert(!details::has_duplicate_shaders<Compiled...>(),
			              "multiple shaders of the same type are not allowed");
			static_assert(details::has_shader_type<GL::GL_VERTEX_SHADER, Compiled...>(), "a vertex shader is required");
			static_assert(details::has_shader_type<GL::GL_FRAGMENT_SHADER, Compiled...>(),
			              "a fragment shader is required");

			program_handle handle;

			{
				auto attach = [&handle](GL::GLuint shader) {            //
					GL::glAttachShader(handle, shader);                 //
					if (GL::glGetError() == GL::GL_INVALID_OPERATION) { //
						throw attach_error{};                           //
					}                                                   //
				};                                                      //

				auto _ = {(attach(shaders.id()), 0)...};
			}

			using VertexShader =
			    typename decltype(details::get_shader_by_type<GL::GL_VERTEX_SHADER, Compiled...>())::type;
			using Attributes = typename VertexShader::attributes;
			hana::for_each(mpl::to_hana<Attributes>, [&handle](auto attr_type) {
				using Attribute = typename decltype(attr_type)::type;
				GL::glBindAttribLocation(handle, Attribute::index, Attribute::name::data());
			});

			GL::glLinkProgram(handle);
			GL::GLint result;
			GL::glGetProgramiv(handle, GL::GL_LINK_STATUS, &result);

			{
				auto detach = [&handle](GL::GLuint shader) { //
					GL::glDetachShader(handle, shader);      //
				};                                           //

				auto _ = {(detach(shaders.id()), 0)...};
			}

			if (static_cast<GL::GLboolean>(result) != GL::GL_TRUE) {
				throw link_error{link_log(handle)};
			}

			using Uniforms = mpl::merge<typename Compiled::uniforms...>;
			using Textures = mpl::filter<is_uniform_texture, Uniforms>;
			using Program  = program<Attributes, mpl::unique<Uniforms>, mpl::unique<Textures>>;
			return Program{std::move(handle)};
		}
	}
}
