#pragma once
#include "details/typestring.h"
#include "gl_impl.h"
#include <boost/hana.hpp>

namespace burpgl {
	namespace hana = boost::hana;

	namespace attributes {
		// an enum used to describe the type of the attribute; it is used
		// internally to select a compile time the correct opengl function to
		// call according to the attribute
		enum class classtype { FLOAT, INTEGER, DOUBLE };

		// `classtypes` maps an attribute type to our classtype
		constexpr auto classtypes = hana::make_map(                        //
		    hana::make_pair(hana::type_c<GL::GLbyte>, classtype::INTEGER), //
		    hana::make_pair(hana::type_c<GL::GLfloat>, classtype::FLOAT),  //
		    hana::make_pair(hana::type_c<GL::GLdouble>, classtype::DOUBLE) //
		    );                                                             //

		// `enums` maps an attribute map to the opengl enum
		constexpr auto enums = hana::make_map(                         //
		    hana::make_pair(hana::type_c<GL::GLbyte>, GL::GL_BYTE),    //
		    hana::make_pair(hana::type_c<GL::GLfloat>, GL::GL_FLOAT),  //
		    hana::make_pair(hana::type_c<GL::GLdouble>, GL::GL_DOUBLE) //
		    );                                                         //

		template <typename T>
		struct attribute_type
		// `attribute_type` provides an uniform interface to query, at compile
		// time, the metadata about an opengl type.
		{
			// the opengl type
			using type = T;

			// the enum needed to refer to the opengl type in the opengl apis
			// static_assert(attribute_type<GLfloat>::value == GL_FLOAT);
			constexpr static GL::GLenum value = enums[hana::type_c<T>];

			// the class type of the attribute
			// static_assert(attribute_type<GLfloat>::class_type == classtype::FLOAT);
			constexpr static classtype class_type = classtypes[hana::type_c<T>];
		};

		template <typename Name, GL::GLuint Index, typename Type, int Components>
		struct base_attribute
		// specializations of `attribute` (`base_attribute` is used to share the
		// code between the various public types) are used to describe the
		// format of an attribute.
		//
		// An attribute is an implicit concept in opengl; a shader defines an
		// attribute, a buffer provides it and a vao must connect the twos.
		// This class is used to explicitly declare an attribute, so that the
		// compiler can statically checks that all the components of a program
		// agree on what is its format.
		//
		// using position = attribute<STYPE("v_position"), 0, GL_FLOAT[3]>;
		//
		// this declaration states that `position` is a vector of three floats;
		// in the shader it is called "v_position" and its location is 0.
		// It's GLSL declarations could looks like:
		//
		// 	in vec3 v_position;
		//
		// 	or even
		// 	layout(location = 0) in vec3 v_position;
		{
			// The name of the attribute stored as a type
			using name = Name;

			// The location assigned to this attribute
			constexpr static auto index = Index;

			// The opengl type of the attribute; it can be simple type
			// (GL_FLOAT) or an array type (GL_FLOAT[3])
			using type = Type;

			// The metadata associated with the opengl type
			using type_data = attribute_type<Type>;

			// The number of elements forming the attribute; it is equal to the
			// size if the type is an array, otherwise it equals to 1
			constexpr static auto components = Components;

			constexpr static GL::GLint size()
			// `size()` calculates the size of the attribute in bytes
			{
				return components * sizeof(type);
			}
		};

	}

	template <typename Name, GL::GLuint Index, typename Type>
	struct attribute : attributes::base_attribute<Name, Index, Type, 1> {};

	template <typename Name, GL::GLuint Index, typename Type, int Components>
	struct attribute<Name, Index, Type[Components]> : attributes::base_attribute<Name, Index, Type, Components> {};

	template <typename>
	struct is_attribute : std::false_type {};

	template <typename T1, GL::GLuint T2, typename T3>
	struct is_attribute<attribute<T1, T2, T3>> : std::true_type {};
}
