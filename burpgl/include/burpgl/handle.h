#pragma once
#include "gl_impl.h"
#include <stdexcept>

namespace burpgl {
	template <typename T>
	class handle {
		/*
		 * Base class for non-copyable handle (unique owner) of a GL resource.
		 *
		 * `T` is the concrete type expected by the GL api.
		 */
	  public:
		handle() : value{} {};
		handle(T v) : value{v} {};
		handle(const handle<T>&) = delete;
		handle(handle<T>&& other) : value{other.value} {
			other.value = T{};
		}
		~handle() = default;

		handle& operator=(const handle<T>&) = delete;

		handle& operator=(handle<T>&& other) {
			value       = other.value;
			other.value = T{};
			return *this;
		}

		operator T() const {
			return value;
		}

		T id() const {
			return value;
		}

	  protected:
		T value;
	};

	using int_handle  = handle<GL::GLint>;
	using uint_handle = handle<GL::GLuint>;

	class handle_error : public std::runtime_error {
		using std::runtime_error::runtime_error;
	};
}
