#include "burpgl/attributes.h"
#include "burpgl/buffers.h"

template<typename...>
class Q;

namespace b = burpgl;

using vertex = b::attribute<STYPE("vertex"), 0, gl::GLfloat[3]>;
using normal = b::attribute<STYPE("normal"), 1, gl::GLfloat[3]>;
using color  = b::attribute<STYPE("color"), 2, gl::GLfloat[3]>;

using b::buffers::offset;

using vertex_buffer = b::buffers::buffer<offset<vertex, 0>, normal>;
using color_buffer = b::buffers::buffer<color>;

