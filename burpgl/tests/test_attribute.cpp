#include "burpgl/attributes.h"
#include "burpgl/uniforms.h"

namespace b = burpgl;

using vertex = b::attribute<STYPE("vertex"), 0, gl::GLfloat[3]>;
static_assert(vertex::type_constant == gl::GL_FLOAT, "type_constant does not match");
static_assert(vertex::components == 3, "components does not match");
static_assert(vertex::size() == 12, "size does not match");

using blending = b::attribute<STYPE("blending"), 1, gl::GLbyte>;
static_assert(blending::type_constant == gl::GL_BYTE, "type_constant does not match");
static_assert(blending::components == 1, "components does not match");
static_assert(blending::size() == 1, "size does not match");

template<typename...>
class Q;

using mvp = b::uniform<STYPE("mvp"), gl::GLfloat[4][4], 1>;
using color = b::uniform<STYPE("color"), gl::GLfloat[3], 1>;

int f() {
	mvp x(0);
	std::array<mvp::type, 1> z;
	x.set(0, z);

	color y(0);
}
//Q<typename mvp::type> x;
