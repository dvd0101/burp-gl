#include "burpgl/program.h"
#include "burpgl/uniforms_samplers.h"

namespace b = burpgl;
namespace mpl = b::mpl;
using namespace gl;

using material_tx = b::textures::texture<STYPE("material"), GL_TEXTURE_2D, GL_RGBA8>;

using vertex   = b::attribute<STYPE("vertex"), 0, gl::GLfloat[3]>;
using material = b::uniform<STYPE("material"), material_tx, 1>;

int main() {
	b::shaders::vertex<vertex> vsh;
	b::shaders::fragment<material> fsh;

	auto r = b::shaders::compile(vsh);
	auto q = b::shaders::compile(fsh);
	auto p = b::shaders::link(r, q);

	using prg_t = decltype(p);
	static_assert(mpl::contains<prg_t::uniforms, material>, "uniform: material not found");
	static_assert(mpl::contains<prg_t::textures, material>, "textures: material not found");
}
