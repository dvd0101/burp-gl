#include "burpgl/attributes.h"
#include "burpgl/program.h"
#include "burpgl/shader.h"
#include <type_traits>
#include <vector>

namespace b = burpgl;

using vertex  = b::attribute<STYPE("vertex"), 0, gl::GLfloat[3]>;
using mvp     = b::uniform<STYPE("mvp"), gl::GLfloat[4][4], 1>;
using lookups = b::uniform<STYPE("lookups"), gl::GLfloat[2][4], 2>;
using counter = b::uniform<STYPE("counter"), gl::GLint, 1>;
using timers  = b::uniform<STYPE("timers"), gl::GLuint, 2>;
using color   = b::uniform<STYPE("color"), gl::GLfloat[3], 1>;
using colors  = b::uniform<STYPE("colors"), gl::GLfloat[3], 2>;

template <typename...>
class Q;

void g() {
	b::shaders::vertex<vertex, mvp> vsh;

	vsh << R"(xxx)";

	b::shaders::fragment<counter, timers, color, colors, mvp, lookups> fsh;

	auto r = b::shaders::compile(vsh);
	auto q = b::shaders::compile(fsh);
	auto p = b::shaders::link(r, q);

	using prg_t = decltype(p);
	static_assert(b::mpl::contains<prg_t::uniforms, mvp>, "uniform: mvp is missing");
	static_assert(b::mpl::contains<prg_t::uniforms, color>, "uniform: color is missing");

	// std::array<gl::GLfloat, 3> red{1, 0, 0};
	p.uniform<counter>(10);
	p.uniform<timers>(10, 0);

	gl::GLuint val1[2];
	p.uniform<timers>(val1);

	std::vector<gl::GLuint> foobar{10, 20, 30, 40};
	p.uniform<timers>(foobar);

	gl::GLfloat red1[3];
	p.uniform<color>(red1);
	std::vector<gl::GLfloat> red2{10, 20, 30};
	p.uniform<color>(red2);
	p.uniform<colors>(red2);

	gl::GLfloat mx1[4][4];
	p.uniform<mvp>(mx1);

	gl::GLfloat mx2[16];
	p.uniform<mvp>(mx2);

	std::vector<float> mx3(16);
	p.uniform<mvp>(mx3);

	std::array<float, 16> mx4;
	p.uniform<mvp>(mx4);

	p.uniform<lookups>(mx1);
	p.uniform<lookups>(mx2);
	p.uniform<lookups>(mx3, 1);
	p.uniform<lookups>(mx4, 1);
}
