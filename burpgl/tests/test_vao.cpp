#include "burpgl/attributes.h"
#include "burpgl/buffers.h"
#include "burpgl/vao.h"

template<typename...>
class Q;

namespace b = burpgl;

using vertex = b::attribute<STYPE("vertex"), 0, gl::GLfloat[3]>;
using normal = b::attribute<STYPE("normal"), 1, gl::GLfloat[3]>;
using color  = b::attribute<STYPE("color"), 2, gl::GLfloat[3]>;

using b::buffers::offset;

using vertex_buffer = b::buffers::array_buffer<offset<vertex, 0>, normal>;
using color_buffer = b::buffers::array_buffer<color>;

using vao = b::vao<vertex_buffer, color_buffer>;

void f() {
	vertex_buffer buff1;
	color_buffer buff2;

	vao v1;
	v1.bind(buff2);
	v1.bind(buff1);
}
