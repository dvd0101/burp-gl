#pragma once
#include <stdexcept>

namespace glcontext {
	class Context_error : public std::runtime_error {
		using std::runtime_error::runtime_error;
	};

	class Context_build_error : public Context_error {
	  public:
		Context_build_error();
	};
}
