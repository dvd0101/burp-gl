#pragma once

namespace glcontext {
	namespace impl {
		template <typename T, typename Tag>
		struct Constant {
		  public:
			explicit constexpr Constant(T v) : value{v} {}

			constexpr bool operator==(Constant o) const {
				return value == o.value;
			}

			constexpr bool operator!=(Constant o) const {
				return value != o.value;
			}

			constexpr operator T() const {
				return value;
			}

		  private:
			T value;
		};
	}
}
