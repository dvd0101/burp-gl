#pragma once
#include "constant.h"

namespace glcontext {
	namespace input {
		using Modifier      = impl::Constant<unsigned short, class _modifier>;
		using Button        = impl::Constant<unsigned char, class _button>;
		using Button_action = impl::Constant<unsigned char, class _button_action>;

		namespace modifiers {
			const Modifier shift(0x0001);
			const Modifier control(0x0002);
			const Modifier alt(0x0004);
			const Modifier super(0x0008);
		}

		namespace buttons {
			const Button one(0);
			const Button two(1);
			const Button three(2);
			const Button four(3);
			const Button five(4);
			const Button six(5);
			const Button seven(6);
			const Button eight(7);

			const Button left(one);
			const Button right(two);
			const Button middle(three);
		}

		namespace button_actions {
			const Button_action press(0);
			const Button_action release(1);
		}
	}
}
