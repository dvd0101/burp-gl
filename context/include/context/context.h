#pragma once
#include "input.h"
#include <functional>
#include <stdexcept>
#include <string>

namespace glcontext {
	class handle;

	enum class Mode { fullscreen, windowed };

	struct Size {
		int width;
		int height;

		float ratio() const {
			return width / height;
		}
	};

	class Context {
	  public:
		Context(Mode);
		Context(Size, Mode);
		Context(Context const&) = delete;
		virtual ~Context();

		Context& operator=(Context const&) = delete;

	  public:
		std::string name() const;
		handle* native_handle();
		Size window_size() const;

	  public:
		virtual void cursor_position(double, double);
		virtual void mouse_button(input::Button, input::Button_action, input::Modifier);
		virtual void scroll(double, double);

	  private:
		handle* _handle;
		Size _window_size;
	};

	void make_current(Context&);

	using render_function = std::function<bool(void)>;
	void draw_forever(Context&, render_function);
}
