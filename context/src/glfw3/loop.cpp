#include "context.h"
#include <GLFW/glfw3.h>

namespace glcontext {

	void draw_forever(Context& ctx, render_function draw) {
		auto window = reinterpret_cast<GLFWwindow*>(ctx.native_handle());
		make_current(ctx);

		glfwSwapInterval(1);
		while (!glfwWindowShouldClose(window)) {
			if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
				glfwSetWindowShouldClose(window, GL_TRUE);
			}

			if (!draw()) {
				break;
			}
			glfwSwapBuffers(window);

			glfwPollEvents();
		}
	}
}
