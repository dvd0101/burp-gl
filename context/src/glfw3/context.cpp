// clang-format off
#include <glbinding/Binding.h>
// clang-format on
#include "../utils.h"
#include "context.h"
#include "exceptions.h"
#include <GLFW/glfw3.h>
#include <algorithm>
#include <cassert>
#include <fmt/format.h>
#include <vector>

namespace {
	void error_callback(int error, const char* description) {
		fmt::print("GLFW error: {} ({})\n", description, error);
	}

	struct Glfw_subsystem {
		Glfw_subsystem() {
			fmt::print("Trying to initialize GLFW: {}...", glfwGetVersionString());
			glfwSetErrorCallback(error_callback);
			if (!glfwInit()) {
				fmt::print("failed\n");
				std::exit(1);
			}
			fmt::print("done\n");
		}

		~Glfw_subsystem() {
			fmt::print("terminating GLFW\n");
			glfwTerminate();
		}
	};

	using glcontext::Context;
	std::vector<Context*> contexts;

	void cursor_position_callback(GLFWwindow* window, double x, double y) {
		auto found = std::find_if(std::begin(contexts), std::end(contexts), [window](auto ctx) {
			auto const w = reinterpret_cast<GLFWwindow*>(ctx->native_handle());
			return w == window;
		});
		assert(found != std::end(contexts));
		(*found)->cursor_position(x, y);
	}

	void button_callback(GLFWwindow* window, int button, int action, int mod) {
		auto found = std::find_if(std::begin(contexts), std::end(contexts), [window](auto ctx) {
			auto const w = reinterpret_cast<GLFWwindow*>(ctx->native_handle());
			return w == window;
		});
		assert(found != std::end(contexts));
		using namespace glcontext::input;
		(*found)->mouse_button(
		    Button(button), action == GLFW_PRESS ? button_actions::press : button_actions::release, Modifier(mod));
	}

	void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
		auto found = std::find_if(std::begin(contexts), std::end(contexts), [window](auto ctx) {
			auto const w = reinterpret_cast<GLFWwindow*>(ctx->native_handle());
			return w == window;
		});
		assert(found != std::end(contexts));
		(*found)->scroll(xoffset, yoffset);
	}

	void connect_callbacks(Context& ctx) {
		contexts.push_back(&ctx);
		auto const window = reinterpret_cast<GLFWwindow*>(ctx.native_handle());

		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		{
			auto const previous = glfwSetCursorPosCallback(window, cursor_position_callback);
			assert(previous == nullptr);
		}
		{
			auto const previous = glfwSetMouseButtonCallback(window, button_callback);
			assert(previous == nullptr);
		}
		{
			auto const previous = glfwSetScrollCallback(window, scroll_callback);
			assert(previous == nullptr);
		}
	}

	void disconnect_callbacks(Context& ctx) {
		auto const window = reinterpret_cast<GLFWwindow*>(ctx.native_handle());
		auto last         = std::remove(std::begin(contexts), std::end(contexts), &ctx);
		assert(last != std::end(contexts));
		contexts.erase(last, std::end(contexts));
		{
			auto const previous = glfwSetCursorPosCallback(window, nullptr);
			assert(previous != nullptr);
		}
		{
			auto const previous = glfwSetMouseButtonCallback(window, nullptr);
			assert(previous != nullptr);
		}
		{
			auto const previous = glfwSetScrollCallback(window, nullptr);
			assert(previous != nullptr);
		}
	}
}

static Glfw_subsystem glfw_handle;

namespace glcontext {
	Context::Context(Mode m) : Context::Context({640, 480}, m) {}

	Context::Context(Size d, Mode m) : _handle{nullptr} {
		glfwWindowHint(GLFW_SAMPLES, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
#if !defined(__APPLE__)
		// OS X GL drivers don't support debug contexts
		glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, 1);
#else
		// OS X only supports forward compatible contexts for GL 3.2+
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

		GLFWmonitor* monitor = nullptr;
		if (m == Mode::fullscreen) {
			monitor                 = glfwGetPrimaryMonitor();
			const GLFWvidmode* mode = glfwGetVideoMode(monitor);

			d.width  = mode->width;
			d.height = mode->height;

			glfwWindowHint(GLFW_RED_BITS, mode->redBits);
			glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
			glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
			glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);
		}
		GLFWwindow* window = glfwCreateWindow(d.width, d.height, "Amaze", monitor, nullptr);
		if (!window) {
			throw Context_build_error{};
		}
		_window_size = d;
		_handle      = reinterpret_cast<handle*>(window);

		make_current(*this);
		glbinding::Binding::initialize();

		connect_callbacks(*this);
	};

	Context::~Context() {
		disconnect_callbacks(*this);
		auto window = reinterpret_cast<GLFWwindow*>(_handle);
		glfwDestroyWindow(window);
	}

	std::string Context::name() const {
		return version();
	}

	handle* Context::native_handle() {
		return _handle;
	}

	Size Context::window_size() const {
		return _window_size;
	}

	void Context::cursor_position(double, double) {}
	void Context::mouse_button(input::Button, input::Button_action, input::Modifier) {}
	void Context::scroll(double, double) {}

	void make_current(Context& ctx) {
		auto window = reinterpret_cast<GLFWwindow*>(ctx.native_handle());
		glfwMakeContextCurrent(window);
	}
}
