#include "debug.h"
#include <fmt/format.h>
#include <fmt/ostream.h>
#include <glbinding/gl/gl.h>

using namespace gl;

namespace {
	struct Debug_message {
		GLenum source_;
		GLenum type_;
		GLuint id_;
		GLenum severity_;

		char const* source() const {
			switch (source_) {
			case GL_DEBUG_SOURCE_API:
				return "GL_DEBUG_SOURCE_API";
			case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
				return "GL_DEBUG_SOURCE_WINDOW_SYSTEM";
			case GL_DEBUG_SOURCE_SHADER_COMPILER:
				return "GL_DEBUG_SOURCE_SHADER_COMPILER";
			case GL_DEBUG_SOURCE_THIRD_PARTY:
				return "GL_DEBUG_SOURCE_THIRD_PARTY";
			case GL_DEBUG_SOURCE_APPLICATION:
				return "GL_DEBUG_SOURCE_APPLICATION";
			case GL_DEBUG_SOURCE_OTHER:
				return "GL_DEBUG_SOURCE_OTHER";
			default:
				return "Unknown";
			}
		}

		char const* type() const {
			switch (type_) {
			case GL_DEBUG_TYPE_ERROR:
				return "GL_DEBUG_TYPE_ERROR";
			case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
				return "GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR";
			case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
				return "GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR";
			case GL_DEBUG_TYPE_PORTABILITY:
				return "GL_DEBUG_TYPE_PORTABILITY";
			case GL_DEBUG_TYPE_PERFORMANCE:
				return "GL_DEBUG_TYPE_PERFORMANCE";
			case GL_DEBUG_TYPE_MARKER:
				return "GL_DEBUG_TYPE_MARKER";
			case GL_DEBUG_TYPE_PUSH_GROUP:
				return "GL_DEBUG_TYPE_PUSH_GROUP";
			case GL_DEBUG_TYPE_POP_GROUP:
				return "GL_DEBUG_TYPE_POP_GROUP";
			case GL_DEBUG_TYPE_OTHER:
				return "GL_DEBUG_TYPE_OTHER";
			default:
				return "Unknown";
			}
		}

		char const* severity() const {
			switch (severity_) {
			case GL_DEBUG_SEVERITY_HIGH:
				return "GL_DEBUG_SEVERITY_HIGH";
			case GL_DEBUG_SEVERITY_MEDIUM:
				return "GL_DEBUG_SEVERITY_MEDIUM";
			case GL_DEBUG_SEVERITY_LOW:
				return "GL_DEBUG_SEVERITY_LOW";
			case GL_DEBUG_SEVERITY_NOTIFICATION:
				return "GL_DEBUG_SEVERITY_NOTIFICATION";
			default:
				return "Unknown";
			}
		}
	};

	std::ostream& operator<<(std::ostream& os, Debug_message const& msg) {
		os << "source=" << msg.source() << " type=" << msg.type() << " id=" << msg.id_
		   << " severity=" << msg.severity();
		return os;
	}

	void context_debug(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei, const GLchar* message,
	                   const void*) {
		Debug_message msg{source, type, id, severity};
		fmt::print("{} - {}\n", msg, message);
	}
}

namespace glcontext {
	void try_log_debug_messages() {
		if (&glDebugMessageCallback != nullptr) {
			glEnable(GL_DEBUG_OUTPUT);
			glDebugMessageCallback(context_debug, nullptr);
		}
	}
}
