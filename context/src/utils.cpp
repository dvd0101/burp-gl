#include "utils.h"
#include <glbinding/gl41core/enum.h>
#include <glbinding/gl41core/functions.h>

namespace glcontext {
	std::string version() {
		return reinterpret_cast<const char*>(gl::glGetString(gl::GL_VERSION));
	}
}
