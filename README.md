# BURP GL

C++ wrapper for OpenGL; declarative and type safe.

# Linux setup

...

# OS X setup

Install a relatively recent compiler. Tested with clang 3.9 .

```bash
brew install glfw glbinding
mkdir build && cd build
conan install .. --build glm Boost
CMAKE_PREFIX_PATH=<homebrew_path>/glbinding/2.1.1_1 cmake .. -DCMAKE_CXX_COMPILER=<path_to_clang>/bin/clang++

#(remove boost_fiber from conanbuildinfo.cmake)

make
```

## DESIGN

Per disegnare qualcosa a schermo abbiamo bisogno di:

1. un framebuffer
1. uno shader
1. una vertex array object (vao)
1. uno o più buffer contenenti i dati che verranno inviati allo shader

Shader e VAO (ma anche i buffer) utilizzano il concetto di `attributo` per
definire il formato delle singole proprietà di un vertice.

Ad esempio il seguente vertex shader:

	in vec3 position;
	in vec3 normal;
	out vec3 f_normal;

	uniform mat4 mvp;

	void main() {
		gl_Position = mvp * vec4(position, 1);
	}

vuole in input vertici formati da due attributi:

* "position" (3 float)
* "normal" (3 float)

La definizione di questi attributi è un concetto pervasivo di tutta
l'applicazione, quindi introduciamo un nuovo tipo

* attribute

che servirà come "collante" a compile time tra shader, vao e buffer

### Attribute

La classe `attribute` viene utilizzata per definire gli attributi utilizzati
dall'applicazione:

	using vertex  = attribute<STYPE("vertex"), 0, GLfloat[3]>;
	using normal  = attribute<STYPE("normal"), 1, GLdouble[3]>;
	using color   = attribute<STYPE("color"), 2, GLbyte[3]>;
	using opacity = attribute<STYPE("opacity"), 3, GLbyte>;

Il primo parametro della specializzazione (`STYPE("vertex")`) è il nome che
l'attributo deve avere in tutti gli shader che lo utilizzano (*questa è una
restrizione rispetto all'api C, ma credo che sia accettabile*).

Il secondo parametro è l'indice dell'attributo che burpgl assegnerà prima della
fase di link di uno shader utilizzando l'api `glBindAttribLocation`.

In realtà non è necessario che gli indici degli attributi sia globalmente
diversi, l'importante è che non ci siano due attributi con lo stesso indice
utilizzati nello stesso contesto (stesso VAOI o buffer). Questo potrebbe
portarci ad avere un API più semplice e flessibile:

using vertex = b::attribute<STYPE("vertex"), GLfloat[3]>; // indice calcolato automaticamente a compile time
using normal = b::attribute<STYPE("normal"), 1, GLfloat[3]>; // indice fisso

### VAO

Il VAO collega gli attributi ai buffer contenenti i dati da passare allo shader:

	using walls = vao<vertex, normal, color>;

Qui l'idea è di creare un tipo che a compile time dichiari quali attributi sono
esposti da un determinato VAO e fare in modo che il compilatore si accorga se
stiamo tentando di utilizzarlo con un programma che ha requisiti diversi:

	using wall_shader = shader<vertex, normal>;
	using outline_shader = shader<vertex>;
	using material_shader = shader<vertex, normal, color, opacity>;

	draw(wall_shader(), walls()); // compiles
	draw(outline_shader(), walls()); // compiles because `walls` exposes more attributes that the ones needed by `outline_shader`
	draw(material_shader(), walls()); // compile error

#### goodbye glVertexAttribPointer

L'api C per descrivere un attributo e mapparlo con una determinata sorgente di
dati `glVertexAttribPointer` in OpenGL 4.3 è stata divisa in due:

* glVertexAttribFormat
* glBindVertexBuffer

questo perché cambiare il formato di un attributo è un operazione costosa,
mentre cambiare la sorgente dei dati è molto veloce [[1-stackoverflow]](http://stackoverflow.com/questions/37972229/#answer-37972230) [[2-stackoverflow]](http://stackoverflow.com/questions/34486197/how-taxing-are-opengl-gldrawelements-calls-compared-to-basic-logic-code/34486405#34486405).

`burp-gl` dovrebbe quindi offrire un api che spinga ad utilizzare opengl nel
modo corretto, ad esempio:

	using vertex_buffer = buffer<
		offset<vertex, 0>,
		offset<normal, vertex::size()>>;

	using color_buffer = buffer<
		offset<color, 0>>;

	using walls = vao<vertex_buffer, color_buffer>;

	vertex_buffer b1 = ...
	color_buffer  b2 = ...
	vertex_buffer bX = ...

	auto vertex_array = walls();

	auto partial = vertex_array.bind(b1);
	auto complete = partial.bind(b2);

	partial = partial.bind(bX); // or vertex_array.bind(bX);

`glVertexAttribFormat` (la parte "costosa" della vecchia
`glVertexAttribPointer`) ha bisogno di consocere l'offset, rispetto all'inizio
del buffer, dove si trova il primo elemento di un attributo; questo implica che
i buffer che possono essere agganciati tramite la `glBindVertexBuffer` devono
avere lo stesso layout.

Questo layout viene codificato nella definizione del template `buffer<>` *api da rivedere*

Il metodo `bind` di un VAO accetta uno o più buffer e ritorna un nuovo tipo
(chiamato come? BoundedVAO?) che codifica al suo interno gli attributi
collegati e di conseguenza può essere utilizzato solo con gli shader
compatibili


### Shader

Esiste un template per ogni shader: vertex, geometry, fragment, etc. I
parametri dei template si utilizzano per descrivere gli attributi utilizzati
(per adesso solo per il vertex shader) ed il numero, nome e tipo degli
uniforms.

Una volta create e inizializzati gli shader, la funzione `link<>()` si
preoccupa di generare un'istanza di `progrm<>` utilizzabile per disegnare. Il
tipo esatto ritornato da `link<>` dipende dagli shader passati e conterrà
l'elenco degli attributi e degli uniforms.

	using color = b::shaders::uniform<GLfloat[3]>;
	using mvp = b::shaders::uniform<GLfloat[4][4]>;

	using outline_vsh = b::shaders::vertex<vertex, mvp>;
	using outline_gsh = b::shaders::geometry<>;
	using outline_fsh = b::shaders::fragment<color>;

	outline_vsh vsh;
	vsh << R"(...)"; // or vsh << ifstream("file.vsh");

	outline_gsh gsh;
	gsh << R"(...)";

	outline_fsh fsh;
	fsh << R"(...)";

	auto outliner = b::shaders::link(vsh, gsh, fsh); // can throw if compile/link errors

	outliner.uniform<mvp>(...); // set the uniform
	outliner.uniform<color>(...);

